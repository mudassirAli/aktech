<?php 

class ProfileC extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}
	
	public function index()
	{		
		if($this->session->userdata('user_id'))
			{
				$user_id= $this->session->userdata('user_id');
				$profile['key'] = $this->user_model->get_by('user_id',$user_id);
				$this->load->view('view_profile',$profile);
		    }
		    else
		    {
		    	redirect('LoginCI/login');
		    }
	}

	public function my_load()
	{
		$config= array();
		$config['upload_path'] = './user_image/';
		$config['allowed_types'] = 'gif|jpg|png';
		$this->load->library('upload',$config);
		
		$this->upload->do_upload('photo');
		$data = $this->upload->data();
		if($data) 
		{
			$image = $data['file_name']; 
			return $image;
		}
	}
	
	public function update_data()
    {
    	$this->form_validation->set_rules('name',' Name','required');
		$this->form_validation->set_rules('pno','Phone Number','required');
		$this->form_validation->set_rules('address','Address','required');
		if ($this->form_validation->run()==false) 
		{
			$this->index();
		}
		else
		{
			if ($_FILES)
			{
				$image = $this->my_load();

			}
				$user_id = $this->input->post('user_id');
				$old_image = $this->input->post('old_image');
				
				$data = array( 
				'user_name' =>$this->input->post('name'),
				'user_phone' => $this->input->post('pno'),
				'user_address' => $this->input->post('address'),
				'image' =>!empty($image)?$image:$old_image
				);
				
				$sessiondata =  array
           		(
					'user_name' =>$this->input->post('name'),
					'image' =>!empty($image)?$image:$old_image
				);

		    	$this->user_model->update_by('user_id', $user_id, $data);
			    $this->session->set_userdata($sessiondata);
	    		redirect('Dashboard/index');
	    }
	}

	public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Password Changed');
		redirect('ProfileC/index');
	}
	public function error_new()
	{
		$this->session->set_flashdata('error_new', 'New Password does not match');
		redirect('ProfileC/index');
	}
	public function error_current()
	{
		$this->session->set_flashdata('error_current', 'Current  Password does not match');
		redirect('ProfileC/index');
	}


	public function change_password()
	{
		$user_id = $this->input->post('user_id');
		$current_password = $this->input->post('cpass');

		$data = $this->user_model->password_compare($current_password,$user_id);
		if(isset($data))
		{
			$npass = $this->input->post('npass');
			$rpass = $this->input->post('rpass');
			if ($rpass==$npass) 
			{
				$update_password = array(
					'user_password' => $npass
				);
				$this->user_model->update_by('user_id',$user_id,$update_password);
				$this->success();
			}
			else
			{
				$this->error_new();
			}
		}
		else
		{
			$this->error_current();
		}		
	}

}

?>