<?php 

class FeedbackC extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('feedback_model');
		$this->load->model('FollowUp_model');
		$this->load->library('form_validation');
	}

	public function index()
	{		
		$this->load->view('feedback');
	}

	public function show_feedback($id)
	{
		$data['a'] = $this->feedback_model->select_edit($id);
		$this->load->view('feedback',$data);
	}
	
	public function addFeedback()
	{		
		$this->load->view('add_feedback');
	}

	public function fetch_feeback_id($id)
	{
		$dataa['b'] = $this->feedback_model->select_edit($id);

		$this->load->view('add_feedback',$dataa);
	}

	public function insert_feedback()
	{

		$id = $this->input->post('id');
		$dataFollowup=array(
			'followup_lastfeedback' => $this->input->post('feedback'),
			'follow_up_date' => $this->input->post('date'),
			'follow_up_time' => $this->input->post('time')
		);
		$this->FollowUp_model->update_by('followup_id',$id,$dataFollowup);
		$user_id = $this->session->userdata('user_id');
		$data = array(
		 'feedback' => $this->input->post('feedback'),
		 'followup_id' => $this->input->post('id'),
		 'feedback_time' => $this->input->post('time'),
		 'feedback_date' => $this->input->post('date'),
		 'user_id' => $user_id
		);
		$this->feedback_model->insert_feedback('follow_up_feedback',$data);
		$this->success($id);
	}
	
	public function success($id)
	{
		$this->session->set_flashdata('success', 'Feedback Inserted');
		redirect('FeedbackC/show_feedback/'.$id);

	}

}
?>