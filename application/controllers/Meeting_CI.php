<?php 
class Meeting_CI extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->model('Meeting_Model');
		$this->load->model('FollowUp_model');
		$this->load->library('form_validation');
		$this->load->helper('my_helper');
	}
	public function index()
	{
		
		$this->load->view('reports/excel');
		
	}

	public function exceldata($data)
	{
		
		header('Content-Type:application/vnd.ms-excel');
		header('Content-disposition:attachment; filename='.rand().'.xls');
		echo $_GET['data'];

		
	}

	public function print_pdf()
	{
		
		$this->load->library('pdf');
		$this->fpdf->SetFont('Arial','B',40);
		$this->fpdf->Cell(40,10,'Hello World!');
		echo $this->fpdf->Output('AktechReport.pdf','D');
	}

	public function get_data_for_update($id)
	{

        $data['key']=$this->Meeting_Model->get_all_where('metting_id',$id);
        $this->load->view('meeting_update',$data);
	}
	
 public function meeting_form_load()
 {
 	
        $this->load->view('meeting_form');
 }
 public function assign_meeting($code)
 {
 	$status = array(
 		'status'=>1,
 	);
 	$this->FollowUp_model->update_by('followup_id',$code,$status);
 	$id=$code;
 	$data['key']=$this->Meeting_Model->select_specific_colum2('follow_up','followup_id',$id);
 	$data['key2']=$this->Meeting_Model->select_specific_colum('users','role','manager');
 	$this->load->view('Meeting_module/assign_meeting',$data);
	
 }


 public function show_all_feedback($code)
       {

       	  $data['key2']=$code;
          $data['key']=$this->Meeting_Model->show_fedback($code);
          $this->load->view('Meeting_module/showfeedback',$data);
       }

public function insert_feedback()
	{
		 $id=$this->input->post('id');
		 $data1 =array
		 (
		  'Date' =>$this->input->post('date'),
		  'Time' =>$this->input->post('time'),
		  'status' =>0);
         $this->Meeting_Model->update_by('metting_id',$id,$data1);
		 $data = array(
		 'feedback' => $this->input->post('feedback'),
		 'meeting_id' => $this->input->post('id'),
		 'user_id' => $this->input->post('list'),
		 'feedback_time' => $this->input->post('time'),
		 'feedback_date' => $this->input->post('date')
		);
		$this->Meeting_Model->save_commit('meeting_feedbacks',$data);
		redirect('Meeting_CI/show_all_feedback/'.$id);
		

	}


       public function show_feedback_form($id)
	{
        $data['key']=$id;
        $data['key2']=$this->Meeting_Model->select_specific_colum('users','role','admin');
		$this->load->view('Meeting_module/addfeedback',$data);
	} 

	public function all_meeting()
	{
		if($this->session->userdata('user_email') && $this->session->userdata('role'))
		{
		$this->load->view('Meeting_module/all_meeting');
	    }
	    else
	    {
	    	redirect('LoginCI/login');
	    }
	}
      public function get_follow_data()
      {
      	$data['key']=$this->Meeting_Model->select_specific_colum('follow_up');
      	$this->load->view('meeting_form',$data);

      }
      
	public function dashboard()
	{
		$this->load->view('main_dashboard');
	}
       public function missed_meeting()
       {
          $this->load->view('Meeting_module/missed_meeting');
       } 
       public function meeting_feedback()
       {
          $this->load->view('Meeting_module/meeting_feedback');
       }

       public function today_meeting()
       {
          $this->load->view('Meeting_module/today_meeting');
       }
	public function delete_meeting_record()
	{

	$id = $_POST['delete'];
	$data = $this->Meeting_Model->delete_data('metting_id',$id);
	if($data)
	{
	$msg = "Delete Meeting data Successfully";
	$response['status'] = true;
	$response['message'] = $msg;
	}
	else
	{
	$msg = "Issue in Deleting Data";
	$response['status'] = false;
	$response['message'] = $msg;	
	
	}

	echo json_encode($response);
	exit;
   
	}



	public function update_meeting_record()
	{
		    $this->layout = "";
            $id=$this->input->post('id');
            $data = array(
				'client_name'   =>                  $this->input->post('cname'),
				'client_businees' =>                $this->input->post('business'),
				'meeting_designation' =>            $this->input->post('designation'),
				'client_meeting_location' =>        $this->input->post('address'),
				'client_phone' =>                   $this->input->post('phone'),
				'client_meeting_date' =>            $this->input->post('date'),
				'client_meeting_time' =>            $this->input->post('time'),
				'client_meeting_feedback' =>        $this->input->post('feedback')
				
			);
			$this->Meeting_Model->update_by('client_meeting_id',$id,$data);
			$this->session->set_flashdata('msg','sdfas');
			redirect('Meeting_CI/meeting_datatables');
			
  
	}
	public function client_meeting()
	{

		$data = array(
		'followup_id'      =>                  $this->input->post('follow_id'),
		'user_id'          =>                  $this->input->post('list'),
		'Date' =>                              $this->input->post('date'),
		'Time' =>                              $this->input->post('time')
				
			);
			$this->Meeting_Model->save_commit('client_meeting',$data);
			$this->session->set_flashdata('msg2','sdfas');
			redirect('Meeting_CI/today_meeting');
			
		}
		
	

	public function view_noification()
 {
   if($_POST["view"]!= '')
 {
 	$data=
 	array(
 		'status'=>1
         );
 	       $this->Meeting_Model->update_commit('client_meeting','status',0,$data);
 }
 $data1=$this->Meeting_Model->select_commit('client_meeting');
 $data2=$this->Meeting_Model->count_notifictaion('client_meeting');
 $output = '';
 if($data1->num_rows()>0)
 {
  foreach ($data1->result() as  $value) 
  {
  	$output .= '
   <li>
    <a>
     <strong>'.$value->Date.'</strong><br />
     <small><em>'.$value->client_name.'</em></small>
    </a>
   </li>';
  }
 }
 else
 {
  $output .= '<li><a href="#" class="text-bold text-italic">Today is rest day</a></li>';
 }
 $data = array(
  'notification'   => $output,
  'unseen_notification' => $data2
 );

 echo json_encode($data);exit();
}

	public function pending_Meeting_datatable()
	{

		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));
		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start+1:$start;
		if($length)
			$this->db->limit($length);
			$this->db->offset($start);
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
			
		}

		if(isset($order[0]['column'])){
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
		}

		$show_data = $this->Meeting_Model->all_meetingdata();
		
		$count_record=count($show_data);
		$response['draw']= $draw;
		$response['recordsTotal']= $count_record;
		$response['recordsFiltered'] = $count_record;
		$response['data'] = $show_data;
		echo json_encode($response);
		exit;
	}
	public function Missed_Meeting_datatable()
	{

		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));
		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start+1:$start;
		if($length)
			$this->db->limit($length);
			$this->db->offset($start);
			$value = '';
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
			
		}

		if(isset($order[0]['column']))
		{
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
		}

		$show_data = $this->Meeting_Model->all_missedmeetingdata($value);
		$count_record=  count($show_data);
		$response['draw']= $draw;
		$response['recordsTotal']= $count_record;
		$response['recordsFiltered'] = $count_record;
		$response['data'] = $show_data;
		echo json_encode($response);
		exit;
	}


	public function today_meeting_datatables()
	{

		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));
		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start+1:$start;
		if($length)
			$this->db->limit($length);
			$this->db->offset($start);
			$value = '';
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
			
		}

		if(isset($order[0]['column']))
		{
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
		}

		$show_data = $this->Meeting_Model->today_meeting($value);
		$count_record=  count($show_data);
		$response['draw']= $draw;
		$response['recordsTotal']= $count_record;
		$response['recordsFiltered'] = $count_record;
		$response['data'] = $show_data;
		echo json_encode($response);
		exit;
	}

	// public function test_data()
	// {
		
	// 	$show_data = $this->Meeting_Model->update_commit('client_meeting','status',0,$data,'CURRENT_DATE()');
	// 	echo $this->db->last_query();
	// 	echo "<pre>";
	// 	print_r($show_data);
	// 	exit();
	// }
	public function new_meeting($id)
	{
		 $data1['key']=$id;
        $data1['key2']=$this->Meeting_Model->select_specific_colum('users','role','admin');
		$this->load->view('new_meeting',$data1);
	}
	public function new_client_meeting()
	{

		$data = array(
		// 'followup_id'      =>                  $this->input->post('follow_id'),
		'user_id'          =>                  $this->input->post('list'),
		'Date' =>                              $this->input->post('date'),
		'Time' =>                              $this->input->post('time')
				
			);
			$this->Meeting_Model->save_commit('client_meeting',$data);
			$this->session->set_flashdata('msg2','sdfas');
			redirect('Meeting_CI/today_meeting');
			
		}


}
?>