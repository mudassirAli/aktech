<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Today_Follow_up extends CI_Controller 
{

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('Today_Follow_model','today_follow');
	    $this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->layout = 'admin/dashboard';
	}

	public function index()
	{
		if($this->session->userdata('user_email') && $this->session->userdata('role'))
		{
			$this->load->view('follow_up/today_follow_up');
	    }
	    else
	    {
	    	redirect('LoginCI/login');
	    }
		
	}

	public function today_follow_up_datatable()
	{

		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start:$start;

		$this->db->limit($length);
		$this->db->offset($start);
		$value = '';
		
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
		}

		if(isset($order[0]['column'])){
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);

			if($this->session->userdata('user_id') && $this->session->userdata('role'))
			{
				$user_id= $this->session->userdata('user_id');
				$role = $this->session->userdata('role');
				if($role == "cro")
					{
						$show_table = $this->today_follow->today_follow_up_show_datatble('follow_up',$value,$user_id);
						$total_data = count($this->today_follow->today_follow_up_show_datatble('follow_up',$value,$user_id));	
					}
					else
					{
						$show_table = $this->today_follow->today_follow_up_admin_show_datatble('follow_up',$value);
						$total_data = count($this->today_follow->today_follow_up_admin_show_datatble('follow_up',$value));
						
					}
				
				$response['draw']= $draw;
				$response['recordsTotal']= $total_data;
				$response['recordsFiltered'] = $total_data ;
				$response['data']=$show_table;
				echo json_encode($response);
				exit;
			}
		}
	}

	public function test_data($id)
	{
		$show_table = $this->today_follow->test_data($id);
		echo "<pre>";
		print_r($show_table);
		exit();
	}


}
?>