<?php

class ProposalC extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('proposal_model');
		$this->layout = 'admin/dashboard';	
		$this->load->library('form_validation');	
	}
	public function index()
	{
		$this->load->view('proposal');
	}
	public function insert()
	{ 
		// $this->form_validation->set_rules('pname','name','required');
		
		$project_type = $this->input->post('webmobile');
		$project_typ = $this->input->post('app');
		$project_ty = $this->input->post('webapp');
		 $id = $this->input->post('id');

		$ar = $project_type.$project_typ.$project_ty;

		// print_r($_POST);
		// exit();

		// if(!empty($this->input->post('ch')))
		// 	{
		// 		$ar =  implode(',', $this->input->post('ch'));
		// 		print_r($ar);
		// 		exit();
		// 	}
		// 	else
		// 		{
		// 			$ar='plz';
		// 		}

		$data = array(	 
		'project_type'           => $ar,
		'project_name'           => $this->input->post('pname'),
		'project_deadline'       =>$this->input->post('deadline'),
		'project_price'          => $this->input->post('budjet'),
		'proposal_date'          => $this->input->post('date'),
		'project_description'    => $this->input->post('pdescription'),
		'followup_id'    => $id
		 );
		$result=$this->proposal_model->insert($data);
	    //$this->load->view('dashboard/main_dashboard');
	}
	public function allproposal()
	{
		$this->load->view('allproposal');
	}
	public function fetch_feeback_id($id)
	{
		// echo $id;
		// exit;
		$dataa['key'] = $this->proposal_model->select_edit1($id);
		// $dataa['key1'] = $this->proposal_model->select_edit2();
		
		$this->load->view('proposal',$dataa);
	}
	public function datatable()
	{
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));
		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start+1:$start;
		if($length)
		{
			$this->db->limit($length);
			$this->db->offset($start);
        }
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
			$this->db->like('client_name',$value,'match');
			$this->db->or_like('client_phone',$value,'match');
			$this->db->or_like('client_business',$value,'match');
			$this->db->or_like('client_address',$value,'match');
			//$this->db->or_like('feedback',$value,'match');
			$this->db->or_like('follow_up_date',$value,'match');
			$this->db->or_like('follow_up_time',$value,'match');
		}
		if(isset($order[0]['column']))
		{
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
		}
		$show_table = $this->FollowUp_model->show_datatble('follow_up');
		$total_data=$this->FollowUp_model->count_data('follow_up');
        $response['draw']= $draw;
		$response['recordsTotal']= $total_data;
		$response['recordsFiltered'] = $total_data;
		$response['data']=$show_table;
		echo json_encode($response);
		exit;
	}
	public function show_pdf()
	{
		$this->load->view('pdf');
	}
	
}

?>