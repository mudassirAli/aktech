<?php 

class Follow_upC extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->model('FollowUp_model');
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}
	
	public function index()
	{	
		if($this->session->userdata('user_id'))
		{
			$user_id = $this->session->userdata('user_id');
			$mdata['key'] = $this->user_model->get_by('user_id',$user_id);
			$this->load->view('follow_up',$mdata);
		}
	}

	public function insert()
	{
		$this->form_validation->set_rules('name',' Name','required');
		$this->form_validation->set_rules('pno','Phone Number','required');
		$this->form_validation->set_rules('business','Business Name','required');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('feedback','Feedback','required');
		$this->form_validation->set_rules('date','Date','required');
		$this->form_validation->set_rules('time','Time','required');
		if ($this->form_validation->run()==false) 
		{
			$this->index();
		}
		else
		{
			$project_type = $this->input->post('webmobile');

			$project_typ = $this->input->post('app');

			$project_ty = $this->input->post('webapp');

			$ar = $project_type.$project_typ.$project_ty;

			$this->db->trans_start();
			$data = array( 
				'client_name' =>$this->input->post('name'),
				'client_phone' => $this->input->post('pno'),
				'application_type' =>  $ar,
				'client_business' => $this->input->post('business'),
				'client_address' => $this->input->post('address'),
				'followup_lastfeedback' => $this->input->post('feedback'),
				'follow_up_date' => $this->input->post('date'),
				'follow_up_time' => $this->input->post('time'),
				'user_id' => $this->input->post('user_id')
			);

			$this->FollowUp_model->save($data);
			$client_id = $this->db->insert_id();

			$array = array('followup_id' => $client_id,'feedback' => $this->input->post('feedback'),'feedback_date' => $this->input->post('date'));

			$this->FollowUp_model->insert_into('follow_up_feedback',$array);

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
				$this->success();
			}

		}	
	}

	public function allFollowUp()
	{
		
		$this->load->view('allFollowUp');
	}

	public function datatable()
	{
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$search=$this->input->get('search');
		$order=$this->input->get('order');
		$columns=$this->input->get('columns');
		$start = $start?$start:$start;


		if($length)
			$this->db->limit($length);
			$this->db->offset($start);
			$value = '';
		if(isset($search['value']) && !empty($search['value']))
		{
			$value = $search['value'];
		}

		if(isset($order[0]['column'])){
			$order_column=$order[0]['column'];
			$order_dir = $order[0]['dir'];
			$column_name = $columns[$order_column]['data'];
			$this->db->order_by($column_name,$order_dir);
		
			if($this->session->userdata('user_id') && $this->session->userdata('role'))
			{
				$user_id= $this->session->userdata('user_id');
				$role = $this->session->userdata('role');
				if($role == "cro")
					{
						$show_table = $this->FollowUp_model->show_datatble('follow_up',$value,$user_id);
						$total_data= count($this->FollowUp_model->show_datatble('follow_up',$value,$user_id));
					}
					else
					{
						$show_table = $this->FollowUp_model->show_admin_datatble('follow_up',$value);
						$total_data= count($this->FollowUp_model->show_admin_datatble('follow_up',$value));
					}
				

				$response['draw']= $draw;
				$response['recordsTotal']= $total_data;
				$response['recordsFiltered'] = $total_data;
				$response['data']=$show_table;
				echo json_encode($response);
				exit;
			}
		}
	}
	
	public function delete($id)
	{
		$this->FollowUp_model->delete_client('followup_id',$id);
		$this->unsuccess();
		redirect('follow_upC/allFollowUp');
	}

	public function edit($id)
	{
		$data['a'] = $this->FollowUp_model->select_edit($id);
		$this->load->view('update_followup',$data);
	}

	public function update_followup()
	{
		$this->load->view('update_followup');
	}

	public function update()
	{
		$id = $this->input->post('id');
		$data = array( 
			'client_name' =>$this->input->post('name'),
			'client_phone' => $this->input->post('pno'),
			'client_business' =>$this->input->post('business'),
			'client_address' => $this->input->post('address'),
			'follow_up_date' => $this->input->post('date'),
			'follow_up_time' => $this->input->post('time')
		);
		$this->FollowUp_model->update_by('followup_id',$id,$data);
		$this->flash_m();
	}

	public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Record Inserted');
		redirect('follow_upC/allFollowUp');
	}

	public function unsuccess()
	{
		$this->session->set_flashdata('success', 'Record Deleted');
		return $this->load->view('allFollowUp');
	}

	public function flash_m()
	{
		$this->session->set_flashdata('success', 'Record updated');
		redirect('follow_upC/allFollowUp');
	}
}


?>		