<?php 

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email') && $this->session->userdata('role'))
		{
			$data['user'] = $this->user_model->get_all();
			$this->load->view('user/index',$data);
	    }
	    else
	    {
	    	redirect('LoginCI/login');
	    }
		
	}

	public function add()
	{
		if($this->session->userdata('user_email') && $this->session->userdata('role'))
		{
			$this->load->view('user/add');
		}
	    else
	    {
	    	redirect('LoginCI/login');
	    }
	}

	public function insert()
	{
		$this->form_validation->set_rules('name',' Name','required');
		$this->form_validation->set_rules('pno','Phone Number','required');
		$this->form_validation->set_rules('email',' Email','required|valid_email|is_unique[users.user_email]');
		$this->form_validation->set_rules('password',' Password','required');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('role','Role','required');
		if ($this->form_validation->run()==false) 
		{
			$this->add();
		}
		else
		{
			if ($_FILES)
			{
				$image = $this->my_load();
			}
			$data = array( 
			'user_name' =>$this->input->post('name'),
			'user_phone' => $this->input->post('pno'),
			'user_email' =>$this->input->post('email'),
			'user_password' =>$this->input->post('password'),
			'user_address' => $this->input->post('address'),
			'role' => $this->input->post('role'),
			'date' => $this->input->post('date'),
			'image' => $image
			);
			$this->user_model->insert_user($data);
			$this->success();
		}	
	}
	
	public function delete($user_id)
    {
       	if($user_id)
       	{
            $this->user_model->delet_data('users',$user_id);
            $this->session->set_flashdata('success_message', 'product has been deleted successfully');
        }
        else
        {
            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
        }
        redirect('User/index');
    }
    
	public function my_load()
	{
		$config= array();
		$config['upload_path'] = './user_image/';
		$config['allowed_types'] = 'gif|jpg|png';
		$this->load->library('upload',$config);
		$this->upload->do_upload('photo');
		$data = $this->upload->data();
		if($data) 
		{
			$image = $data['file_name']; 
			return $image;
		}
	}
	
	public function success()
	{
		$this->session->set_flashdata('success', 'Successfully Record Inserted');
		redirect('Dashboard/dashboard');
	}
	
	public function update($user_id)
    {
       	if(isset($user_id))
       	{
            $data['user'] = $this->user_model->get_by('user_id',$user_id);
			$this->load->view('user/update',$data);
        }
    }
    
    public function update_data()
    {
    	$this->form_validation->set_rules('name',' Name','required');
		$this->form_validation->set_rules('pno','Phone Number','required');
		$this->form_validation->set_rules('email',' Email','required');
		$this->form_validation->set_rules('password',' Password','required');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('role','Role','required');
		$user_id = $this->input->post('user_id');
		if ($this->form_validation->run()==false) 
		{
			$this->update($user_id);
		}
		else
		{
			if ($_FILES)
			{
				$image = $this->my_load();
			}
			$old_image = $this->input->post('old_image');
			$data = array( 

			'user_name' =>$this->input->post('name'),
			'user_phone' => $this->input->post('pno'),
			'user_email' =>$this->input->post('email'),
			'user_password' =>$this->input->post('password'),
			'user_address' => $this->input->post('address'),
			'role' => $this->input->post('role'),
			'date' => $this->input->post('date'),
			'image' =>!empty($image)?$image:$old_image
			);
	    	
	    	$this->user_model->update_by('user_id', $user_id, $data);
	    	redirect('User/index');
	    }
	}

	
}

?>