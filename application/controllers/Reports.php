<?php

class Reports extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('ReportModel','reports_model');
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->layout = "admin/dashboard";
	}

	public function index()
	{	
		if($this->session->userdata('role')=='admin' && $this->session->userdata('user_email'))
		{
				$role = 'cro';
				$role1 = 'manager';
				$data['key'] = $this->reports_model->fetch_cro('users',$role); 
				$data['key1'] = $this->reports_model->fetch_cro('users',$role1); 
				$this->load->view('Reports/index',$data);
		}
		else
		{
			redirect('LoginCI/login');
		}
	}

	public function report()
	{
		if($this->session->userdata('role')=='admin' && $this->session->userdata('user_email'))
		{
			$data = array();
			$from = $this->input->post('from');
			$to = $this->input->post('to');

			$cro_check = $this->input->post('cro');
			$follow_id_check = $this->input->post('follow_id');
			$cro_user_id = $this->input->post('cro_user_id');
		
			$meeting = $this->input->post('meeting_list');
			$meeting_check = $this->input->post('client_meeting');

				if(!empty($cro_user_id && $follow_id_check && $cro_check))
				{
					$data['follow_up_date'] = $this->reports_model->fetch_table_data($from,$to,$cro_user_id); 
				}
				if(!empty($meeting && $meeting_check))
				{
					$data['meeting'] = $this->reports_model->meeting_up($from,$to,$meeting);
				}

			$this->load->view('Reports/view',$data);
		}
		else
		{
			redirect('LoginCI/login');
		}
	}



	
}
?>