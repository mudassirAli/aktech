<?php 

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model','dashboard_model');
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->layout = "admin/dashboard";
		$this->load->model('Meeting_Model');
	}
	public function index()
	{
		if($this->session->userdata('user_id') && $this->session->userdata('role'))
		{
			$user_id= $this->session->userdata('user_id');
			$role = $this->session->userdata('role');

			if($role == "cro")
			{
				$result['todayFollowupCount'] =  count($this->dashboard_model->today_follow_up('follow_up',$user_id));
				$result['missedFollowUpCount'] = count($this->dashboard_model->missed_follow_up('follow_up',$user_id));
				$result['doneFollowUpCount'] = count($this->dashboard_model->done_follow_up('follow_up',$user_id));
				$result['allFollowUpCount'] = count($this->dashboard_model->all_follow_up('follow_up',$user_id));
		
			}
			elseif ($role == "admin")
			{
				$result['todayFollowupCount'] =  count($this->dashboard_model->admin_today_follow_up('follow_up'));
				$result['missedFollowUpCount'] = count($this->dashboard_model->admin_missed_follow_up('follow_up'));
				$result['doneFollowUpCount'] = count($this->dashboard_model->admin_done_follow_up('follow_up'));
				$result['allFollowUpCount'] = count($this->dashboard_model->admin_all_follow_up('follow_up'));
			}
		
			$result['todaymeeting']=count($this->Meeting_Model->today_meeting_count());
			$result['missedmeeting']=count($this->Meeting_Model->missed_meeting_count());

			$this->load->view('dashboard/main_dashboard',$result);
		}
	}
	public function Dashboard()
	{
		if($this->session->userdata('user_id') && $this->session->userdata('role'))
		{
			$user_id= $this->session->userdata('user_id');
			$role = $this->session->userdata('role');

			if($role == "cro")
			{
				$result['todayFollowupCount'] =  count($this->dashboard_model->today_follow_up('follow_up',$user_id));
				$result['missedFollowUpCount'] = count($this->dashboard_model->missed_follow_up('follow_up',$user_id));
				$result['doneFollowUpCount'] = count($this->dashboard_model->done_follow_up('follow_up',$user_id));
				$result['allFollowUpCount'] = count($this->dashboard_model->all_follow_up('follow_up',$user_id));
		
			}
			elseif ($role == "admin")
			{
				$result['todayFollowupCount'] =  count($this->dashboard_model->admin_today_follow_up('follow_up'));
				$result['missedFollowUpCount'] = count($this->dashboard_model->admin_missed_follow_up('follow_up'));
				$result['doneFollowUpCount'] = count($this->dashboard_model->admin_done_follow_up('follow_up'));
				$result['allFollowUpCount'] = count($this->dashboard_model->admin_all_follow_up('follow_up'));
			}
		
			$result['todaymeeting']=count($this->Meeting_Model->today_meeting_count());
			$result['missedmeeting']=count($this->Meeting_Model->missed_meeting_count());

			$this->load->view('dashboard/main_dashboard',$result);
		}
	}
	





}
?>		