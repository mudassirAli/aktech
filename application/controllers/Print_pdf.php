<?php

class Print_pdf extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->load->model('mpdf_model');
    }


    public function index()
    {
   
        $this->load->library('M_pdf');
        
        // retrieve data from model
        $data = [];
        // boost the memory limit if it's low ;)
        $html = $this->load->view('Login/login', $data, true);
        $filepath='Download.pdf';
        $this->M_pdf->pdf->WriteHTML($html);
        // write the HTML into the PDF
        $this->M_pdf->pdf->Output($filepath,'D');
   
    }
    // public function generatePDF()
    // {}
}
/* End of file dashboard.php */
/* Location: ./system/application/modules/matchbox/controllers/dashboard.php */