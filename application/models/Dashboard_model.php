<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Dashboard_model extends Abstract_model {

    protected $table_name = "";

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "";
		parent::__construct();
    }
    // SELECT COUNT(feedback_date) FROM `follow_up_feedback` WHERE feedback_date=CURRENT_DATE()
    public function today_follow_up($table,$user_id)
    {

    	$this->db->select("*");
    	$this->db->from($table);
        $this->db->where('user_id',$user_id);
        $this->db->where('status',0);
    	$this->db->where("follow_up_date = CURRENT_DATE()");
    	$data=$this->db->get();
        return $data->result_array();
	    
    }
    public function missed_follow_up($table,$user_id)
    {
    	$this->db->select("*");
        $this->db->from($table);
        $this->db->where('user_id',$user_id);
        $this->db->where('status',0);
    	$this->db->where("follow_up_date < CURRENT_DATE()");
    	$data=$this->db->get();
	    return $data->result_array();

    }
    
    public function done_follow_up($table,$user_id)
    {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where('user_id',$user_id);
        $this->db->where('status',1);
        // $this->db->where("follow_up_date < CURRENT_DATE()");
        $data=$this->db->get();
        return $data->result_array();
    }
    public function all_follow_up($table,$user_id)
    {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where('user_id',$user_id);
        $data=$this->db->get();
        return $data->result_array();

    }
    public function admin_all_follow_up($table)
    {

        $this->db->select("*");
        $this->db->from($table);
        $data=$this->db->get();
        return $data->result_array();
        
    }
    public function admin_today_follow_up($table)
    {

        $this->db->select("*");
        $this->db->from($table);
        // $this->db->where('user_id',$user_id);
        $this->db->where('status',0);
        $this->db->where("follow_up_date = CURRENT_DATE()");
        $data=$this->db->get();
        return $data->result_array();
        
    }
    public function admin_missed_follow_up($table)
    {
        $this->db->select("*");
        $this->db->from($table);
        // $this->db->where('user_id',$user_id);
        $this->db->where('status',0);
        $this->db->where("follow_up_date < CURRENT_DATE()");
        $data=$this->db->get();
        return $data->result_array();

    }
     public function admin_done_follow_up($table)
    {
        $this->db->select("*");
        $this->db->from($table);
        // $this->db->where('user_id',$user_id);
        $this->db->where('status',1);
        // $this->db->where("follow_up_date < CURRENT_DATE()");
        $data=$this->db->get();
        return $data->result_array();

    }
 
	
}
?>