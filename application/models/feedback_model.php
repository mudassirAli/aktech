<?php

include_once('abstract_model.php');

class feedback_model extends Abstract_model
{
	
	protected $table_name = "";
	
	public function __construct() 
	{
		$this->table_name = "follow_up_feedback";
		parent::__construct();
	}
	public function select_edit($id)
	{
		$this->db->select('*');
		$this->db->from('follow_up');
		$this->db->join('follow_up_feedback','follow_up_feedback.followup_id = follow_up.followup_id');
		$this->db->where('follow_up.followup_id',$id);
		$this->db->order_by("follow_up_feedback.feedback_id","desc");
		$data=$this->db->get();
		return $data->result_array();
	}
	public function insert_feedback($table,$data)
	{	
	   $this->db->insert($table,$data);
	   // $this->db->where('followup_id',$id);
	}
	
}
?>     