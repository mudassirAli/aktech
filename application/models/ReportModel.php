<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package      Admin Pannel  
* @subpackage   Model
* @author       Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link         http://
*/

include_once('Abstract_model.php');

class ReportModel extends Abstract_model
{
    /**
    * @var stirng
    * @access protected
    */
    protected $table_name = "";
    
    /** 
    *  Model constructor
    * 
    * @access public 
    */
    public function __construct() 
    {
        parent::__construct();
        $this->table_name = "";
    }

    // public function reportsByDate($from,$to,$Purchase,$PurchaseReturn,$sales,$product)
    // {
        
    //     $this->db->select("*");
    //     $this->db->from('products');
    //     if(!empty($Purchase))
    //     {
    //         $this->db->join('purchases','purchases.bar_code = products.bar_code');
    //         $this->db->where("purchases.purchase_date BETWEEN '$from' AND '$to'");
    //     }
    //     if(!empty($PurchaseReturn))
    //     {
    //         $this->db->join('purchase_return','purchase_return.bar_code = products.bar_code');
    //         $this->db->where("purchase_return.purchase_return_date BETWEEN '$from' AND '$to'"); 
    //     }

    //     if(!empty($sales))
    //     {
    //         $this->db->join('sales','sales.bar_code = products.bar_code');
    //         $this->db->where("sales.sales_date BETWEEN '$from' AND '$to'"); 
    //     }
    //     if(!empty($product))
    //     {
    //         $this->db->where("products.product_date BETWEEN '$from' AND '$to'"); 
    //     }
    //     //   if(!empty($product || $sales))
    //     // {
    //     //       // $this->db->join('products','products.bar_code = sales.bar_code');
    //     //      // $this->db->where("products.product_date BETWEEN '$from' AND '$to'");  
    //     //     $this->db->where("sales.sales_date BETWEEN '$from' AND '$to'");
            
    //     // }
    //     $this->db->order_by('product_id','DESC');
    //     $query = $this->db->get();
    
    //     return $query->result_array();
    // }

    public function fetch_cro($table,$role)
    {
        $this->db->where('role',$role);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function fetch_table_data($from,$to,$cro_user_id)
    {
        $this->db->select("*");
        $this->db->from('follow_up');
        $this->db->join('users','users.user_id = follow_up.user_id');
        $this->db->where('follow_up.user_id',$cro_user_id);
        $this->db->where("follow_up.follow_up_date BETWEEN '$from' AND '$to'"); 
        $this->db->order_by('follow_up.follow_up_date','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function meeting_up($from,$to,$meeting)
    {
        $this->db->select("*");
        $this->db->from('follow_up');
        $this->db->join('client_meeting','client_meeting.followup_id = follow_up.followup_id');
        $this->db->join('users','users.user_id = client_meeting.user_id');
        $this->db->where('client_meeting.user_id',$meeting);
        $this->db->where("client_meeting.Date BETWEEN '$from' AND '$to'"); 
        $this->db->order_by('client_meeting.Date','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

     

}