<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

 include_once('Abstract_model.php');

class Missed_Follow_model extends Abstract_model
{
	/**
	// * @var stirng
	// * @access protected
	*/
    protected $table_name = "";
	
	// * 
	// *  Model constructor
	// * 
	// * @access public 
	
  public function __construct() 
	{
    $this->table_name = "";
	  parent::__construct();
  }
  public function show_Admin_missed_datatble($table,$value)
  {

   $this->db->select("*");
    $this->db->from($table);
    $this->db->join('users','users.user_id = follow_up.user_id');
    $this->db->where('status',0);
    $this->db->where("follow_up_date < CURRENT_DATE()");
      if(!empty($value))
      {
        $this->db->where(" (
          follow_up.client_name like '%$value%' OR
          follow_up.client_phone like '%$value%' OR
          follow_up.client_business like '%$value%' OR
          follow_up.client_address like '%$value%' OR
          follow_up_feedback.feedback like '%value%' OR
          follow_up_feedback.feedback_date like '%$value%' OR
          follow_up_feedback.feedback_time like '%$value%'
          ) ");
      }
      $query = $this->db->get();
      return $query->result();  
  } 

  public function missed_follow_show_datatble($table,$value,$user_id)
  {
  	$this->db->select("*");
  	$this->db->from($table);
    $this->db->where('user_id',$user_id);
    $this->db->where('status',0);
    $this->db->where("follow_up_date < CURRENT_DATE()");
      if(!empty($value))
      {
        $this->db->where(" (
          follow_up.client_name like '%$value%' OR
          follow_up.client_phone like '%$value%' OR
          follow_up.client_business like '%$value%' OR
          follow_up.client_address like '%$value%' OR
          follow_up.application_type like '%$value%' OR
          follow_up.follow_up_date like '%$value%' OR
          follow_up.follow_up_time like '%$value%'
          ) ");
      }
  	$query = $this->db->get();
   	return $query->result(); 
  }

  

}
?>