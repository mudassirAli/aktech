<?php

include_once('abstract_model.php');

class Meeting_Model extends Abstract_model
{
	
	function __construct()
	{
		
            $this->table_name = "client_meeting";
		    parent::__construct();

	}
	public function all_meetingdata()
	{
		$this->db->select("*");
		$this->db->from('client_meeting ');
		$this->db->join('follow_up ','client_meeting.followup_id=follow_up.followup_id');
        $this->db->join('users','client_meeting.user_id=users.user_id');
		$this->db->order_by('metting_id','DESC');
		$result=$this->db->get();
		return $result->result();

	}

	public function show_fedback($id)
	{
		$this->db->select("meeting_feedbacks.created_at,follow_up.client_name,meeting_feedbacks.feedback,meeting_feedbacks.feedback_date,meeting_feedbacks.feedback_time,meeting_feedbacks.meeting_id,users.user_name");
		$this->db->from('meeting_feedbacks');
		$this->db->join('client_meeting','client_meeting.metting_id=meeting_feedbacks.meeting_id');
		$this->db->join('follow_up','follow_up.followup_id=client_meeting.followup_id');
		$this->db->join('users','users.user_id=meeting_feedbacks.user_id');
        $this->db->where("meeting_feedbacks.meeting_id=$id");
        $this->db->order_by('feedback_id','desc');
        $result=$this->db->get();
		return $result->result_array();
	}

	
	public function all_missedmeetingdata($value)
	{
		
		$this->db->select("*");
		$this->db->from('client_meeting');
		$this->db->join('follow_up','client_meeting.followup_id=follow_up.followup_id');
        $this->db->join('users','client_meeting.user_id=users.user_id');
        $this->db->where('Date < CURRENT_DATE()');
        $this->db->order_by('metting_id','DESC');
		if(!empty($value))
		{
			$this->db->where("(
		     follow_up.client_name like '%$value%' OR
		     follow_up.client_business like '%$value%' OR
		     follow_up.followup_designation like '%$value%' OR
		     follow_up.client_address like '%$value%' OR
		     follow_up.client_phone like '%$value% 'OR
		     users.user_name like '%$value%' OR
		     client_meeting.Date like '%$value%' OR
		     client_meeting.Time like '%$value%'     
			 )
		");
		}
		$result=$this->db->get();
		return $result->result();


	}

	public function today_meeting($value)
	{
		
		$this->db->select("*");
		$this->db->from('client_meeting');
		$this->db->join('follow_up','client_meeting.followup_id=follow_up.followup_id');
        $this->db->join('users','client_meeting.user_id=users.user_id');
        $this->db->where('client_meeting.Date = CURRENT_DATE()');
		if(!empty($value))
		{
			$this->db->where("(
		     follow_up.client_name like '%$value%' OR
		     follow_up.client_business like '%$value%' OR
		     follow_up.followup_designation like '%$value%' OR
		     follow_up.client_address like '%$value%' OR
		     follow_up.client_phone like '%$value% 'OR
		     users.user_name like '%$value%' OR
		     client_meeting.Date like '%$value%' OR
		     client_meeting.Time like '%$value%'     
			 )
		");
		}
		$result=$this->db->get();
		return $result->result();
	}


public function today_meeting_count()
    {

    	$this->db->select();
    	$this->db->from($this->table_name);
    	$this->db->where('Date = CURRENT_DATE()');
    	$data=$this->db->get();
       	return $data->result_array();
	
		
    }

    public function missed_meeting_count()
    {

    	$this->db->select();
    	$this->db->from($this->table_name);
    	$this->db->where('Date < CURRENT_DATE()');
    	$data=$this->db->get();
       	return $data->result_array();
	
		
    }

	  public function delete_data($column,$id) 
      {
        $this->db->where($column,$id);
        return $this->db->delete($this->table_name);
      }
	public function count_data()
	{
		$this->db->select();
		$this->db->from($this->table_name);
		return $this->db->count_all_results();
	}
	public function save_commit($table,$data)
	{
		$this->db->insert($table,$data);
		return true;
	}

	public function update_commit($table_name,$field,$fielddata,$data)
	{
	
		$this->db->where("$field=$fielddata AND Date=CURRENT_DATE()");
		$this->db->update($table_name,$data);
	}

	public function select_commit($table)
	{
		$this->db->select();
		$this->db->from('client_meeting');
		$this->db->join('follow_up','client_meeting.followup_id=follow_up.followup_id');
        $this->db->join('users','client_meeting.user_id=users.user_id');
        $this->db->where("Date=CURRENT_DATE()");
        $this->db->order_by("metting_id", "DESC");
        $this->db->limit(5);
        return $this->db->get();
         
	}

	public function get_feedback($table,$id)
	{
		$this->db->select();
		$this->db->from('meeting_feedbacks');
		$this->db->join('client_meeting','client_meeting.metting_id=follow_up_feedback.meeting_id');
        $this->db->join('follow_up','follow_up.followup_id=follow_up_feedback.followup_id');
        $this->db->where("follow_up_feedback.meeting_id=$id");
        $this->db->order_by("metting_id", "DESC");
        $this->db->limit(5);
        return $this->db->get();
         
	}
	public function count_notifictaion($table_name)
	{   
		$this->db->select();
		$this->db->where('status=0 AND Date=CURRENT_DATE()');
		$this->db->from($table_name);
		$data=$this->db->get();
		return $data->num_rows();
		
	}


public function select_specific_colum($table_name,$where,$id)
{
     return $this->db->select()->from($table_name)->where($where,$id)->get()->result();
       
}
public function select_specific_feedback($table_name,$where,$id)
{
     $this->db->select();
		$this->db->from('client_meeting');
		$this->db->join('follow_up','client_meeting.followup_id=follow_up.followup_id');
        $this->db->where("follow_up_feedback.meeting_id=$id");
        return $this->db->get();
       
}
public function select_specific_colum2($table_name,$where,$id)
	{
     $this->db->where($where,$id);
      $data= $this->db->get($table_name);
     if($data->result())
     {
     	return $data->row();
     }
       
	}

	// public function select_all($table)
	// {

	// }

	
}
?>