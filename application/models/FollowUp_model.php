<?php 

include_once('abstract_model.php');

//protected $table_name = "";

class FollowUp_model extends Abstract_model 
{
  

	public function __construct() 
	{
    $this->table_name = "follow_up";
    parent::__construct();
  }
   public function show_admin_datatble($table,$value)
  {
    $this->db->select("*");
    $this->db->from($table);
    $this->db->join('users','users.user_id = follow_up.user_id');
    // $this->db->where('user_email',$email);
    // $this->db->where('role',$role);
    if(!empty($value))
      {
        $this->db->where(" (
          follow_up.client_name like '%$value%' OR
          follow_up.client_phone like '%$value%' OR
          follow_up.client_business like '%$value%' OR
          follow_up.client_address like '%$value%' OR
          follow_up.application_type like '%$value%' OR
          follow_up.followup_lastfeedback like '%$value%' OR
          follow_up.follow_up_date like '%$value%' OR
          follow_up.follow_up_time like '%$value%'
          ) ");
      }
    $this->db->order_by('followup_id','DESC');
    $query = $this->db->get();
    return $query->result(); 
  } 
  public function show_datatble($table,$value,$user_id)
  {
    $this->db->select("*");
    $this->db->from($table);
    // $this->db->join('users','users.user_id = follow_up.user_id');
    $this->db->where('user_id',$user_id);
    $this->db->where('status',0);
    if(!empty($value))
      {
        $this->db->where(" (
          follow_up.client_name like '%$value%' OR
          follow_up.client_phone like '%$value%' OR
          follow_up.client_business like '%$value%' OR
          follow_up.client_address like '%$value%' OR
          follow_up.application_type like '%$value%' OR
          follow_up.followup_lastfeedback like '%$value%' OR
          follow_up.follow_up_date like '%$value%' OR
          follow_up.follow_up_time like '%$value%'
          ) ");
      }
    $this->db->order_by('followup_id','DESC');
    $query = $this->db->get();
    return $query->result(); 
  } 

  public function count_data($table)
  {
    $this->db->select();
    $this->db->from($table);
    return $this->db->count_all_results();
  }
  public function select_edit($id)
  {
    $this->db->where('followup_id',$id);
    $data=$this->db->get('follow_up');
    return $data->result_array();
  }


  public function insert_into($table,$array)
  {
    $this->db->insert($table,$array);
    return true;
  }
  public function delete_client($column,$id)
  {
    $this->db->where($column,$id);
    $this->db->delete($this->table_name);
  }
}
?>