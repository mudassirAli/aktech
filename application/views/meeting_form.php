<style type="text/css">
    .error{color: red};
</style>
<script src="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.css">
   
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                       
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                           
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                 
                    <div class="row">
                        <div class="col-md-12">
                           
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Follow Up -
                                            
                                        </span>
                                    </div>
                                
                                </div>
                            
<form class="form-horizontal" action="<?php echo base_url('Meeting_CI/client_meeting');?>" method="POST">
                                    
                                            <div class="form-body">
                                               
                                           

                                                <div class="tab-content">
                                                    
                                                    
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Provide your account details</h3>

                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Client Name
                                                               
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="business" placeholder="Provide your Business name" />
                                                              
                                                            </div>
                                                        </div><br>
                                                      
                                                      
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Business
                                                               
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="business" placeholder="Provide your Business name" />
                                                              
                                                            </div>
                                                        </div><br>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Manager
                                                              
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="manager" placeholder="Provide your Business name" />
                                                              
                                                            </div>
                                                        </div><br>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Designation
                                                           <!--      <span class="required"> * </span> -->
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="designation" placeholder="Provide your Business name" />
                                                               
                                                            </div>
                                                        </div><br>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Address
                                                             
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="address" placeholder="Provide your Address" />
                                                               
                                                            </div>
                                                        </div><br>
                                                          <div class="form-group">
                                                            <label class="control-label col-md-3">Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="Phone" class="form-control" name="phone" placeholder="347-454-9789"  />
                                                                <span class="error"><?php echo form_error('phone')?></span>
                                                            </div>
                                                        </div><br>
                                                       
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date
                                                              
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="date" class="form-control" name="date" />
                                                              
                                                            </div>
                                                        </div><br>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Time
                                                              
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="time" class="form-control" name="time"  />
                                                               
                                                            </div>
                                                        </div>
                                                         
                                               </div>
                                                </div>
                                                <input type="submit" name="" class="btn btn-success pull-right" style="text-align: center;">
                                            </div>

                                        </form>                                
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>

        



