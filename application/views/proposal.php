<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
  <!-- BEGIN PAGE HEAD-->
  <div class="page-head">
    <div class="page-toolbar">
    </div>
  </div>

  <div class="row">
    <div class="portlet box blue-hoki">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-gift"></i>Proposal </div>
        </div>
        <div class="portlet-body form">
          <!-- BEGIN FORM-->
          <form class="form-horizontal" action="<?php echo base_url('ProposalC/insert');?>" method="POST">
            <div class="form-body">
              <div class="facilities">

                <div class="form-group" style="margin-left: 280px">
                 <!--  <label>Select Project type</label><br> -->
                 <!-- <select id="framework" name="framework[]" multiple class="form-control" > -->
                  <div id="a">
                   <input type="checkbox" name="proposal" required="">
                   <label>Select project type</label>
                 </div>
                 <div id="b" style="display: none;">
                   <input type="radio" name="webmobile" value="Website ">
                   <label>web</label>&nbsp
                   <input type="radio"  id="f" name="webmobile" value="Mobile Application " >
                   <label>mobile app</label>&nbsp
                   <input type="radio"  name="webmobile" id="d">
                   <label>website and mobile app </label>
                 </div>

                 <div id="e" style="display: none;">
                  <input type="radio" name="app" value="IOS" >
                  <label>ios</label>&nbsp&nbsp
                  <input type="radio" name="app" value="Andriod app" >
                  <label>andriod</label>&nbsp&nbsp
                  <input type="radio" name="app" value="Hybrid">
                  <label>Hybrid</label>
                </div>

                <div id="g" style="display: none;">
                  <input type="radio" name="webapp" value="Website And IOS" >
                  <label>ios</label>&nbsp&nbsp
                  <input type="radio" name="webapp" value="Website And Andriod app" >
                  <label>andriod</label>&nbsp&nbsp
                  <input type="radio" name="webapp" value="Website And Hybrid">
                  <label>Hybrid</label>
                </div>
                <!-- </select> -->
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Project Name</label>
                <div class="col-md-4">

                  <?php foreach ($key as $value) 
                  {
                    $id=$value['followup_id'];
                    //$idd=$value['user_id'];

                    ?>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <input type="hidden" name="ww" value="<?php // echo $idd; ?>">

                    <?php }?>

                    <input type="text" class="form-control" placeholder="Enter Project Name" name="pname" required="">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-3 control-label">Project Deadline</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Enter Deadline for this Project" name="deadline" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Total Budget</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" placeholder="Enter totall Budjet of this Project" name="budjet" required="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Project Description</label>
                  <div class="col-md-4">
                   <textarea type="text" class="form-control" placeholder="Enter Project description" name="pdescription" required=""></textarea> 
                 </div>
               </div>
               <div><input type="hidden" name="date" value="<?php echo date('y-m-d');?>"></div>
               <div class="form-group">
                <div class="row">
<!--                   <div class="col-md-2"></div> -->
                  <div class="col-md-offset-3 col-md-4">
                    <button type="submit" class="btn green">Submit</button>
                    <button type="button" class="btn green pull-right" onclick="pdfFunction()">Attach PDF</button></div>
                    <div class="col-md-8"></div>
                   
                </div>
              </div>
            </div>

          </div>
        </form>
        <!-- END FORM-->
      </div>
    </div>
  </div>
  <!-- END PAGE BASE CONTENT -->
</div>






<script type="text/javascript">
  $(document).ready(function(){
    $('#a').on('click',function(){
      $("#b").toggle();
    });

    $('#f').click(function(){
      $('#e').toggle();
    }); 

    $('#d').click(function(){
      $('#g').toggle();
    }); 
  });
  function pdfFunction()
  {
    window.print();
  }  
</script>







