<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo base_url('assets/jquery.table2excel.js')?>"></script> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script> 
 <script src="<?php echo base_url('assets/faker.min.js')?>"></script>
 <script src="<?php echo base_url('assets/jspdf.debug.js')?>"></script>
 <script src="<?php echo base_url('assets/jspdf.plugin.autotable.js')?>"></script>
 <script src="<?php echo base_url('assets/jspdf.plugin.autotable.min.js')?>"></script>
 <script src="<?php echo base_url('assets/jspdf.plugin.autotable.src.js')?>"></script>
 <script type="text/javascript">
    function run_pdf()
    {

        var doc = new jsPDF();
        var res = doc.autoTableHtmlToJson(document.getElementById("Reports"));
        var header = function(data)
        {
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle('normal');
            //doc.addImage(headerImgData, 'png', data.settings.margin.left, 20, 50, 50);
            doc.text("Report", data.settings.margin.left, 15);
        };

        var options = {
            addPageContent : header,
            margin: {
            top: 80
            },
            startY: doc.autoTableEndPosY() + 20
        };

        doc.autoTable(res.columns, res.data, options);

        doc.save("Report.pdf");
    }
</script>

 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Untitled Document</title>
    <style>
</style>
</head>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light" >
            <img src="<?php echo base_url()?>assets/layouts/layout4/img/aktech.png" style="width: 100px">
            <div class="portlet-title" >
                <div cLass="caption">
                    <span class="caption-subject font-green-sharp bold uppercase">
                    Reports History
                    </span>
                </div>
            </div>
            <?php
                if(isset($follow_up_date))
                {
                    ?>
            <table class="table table-striped table-bordered table-hover" id="Reports"  >
                <thead>
<h3 class="bold" style="text-align: center;">Meeting History Report</h3>
                   <!--  <tr>
                        <td colspan="6">
                            <h3 class="bold" style="text-align: center;">Follow_up  History Report</h3>
                        </td>
                    </tr> -->
                    <tr>
                        
                        <th class="bold" width="10%">Date</th>
                        <th class="bold" width="10%">Cro</th>
                        <th class="bold" width="15%" >Client Name</th>
                        <th class="bold" width="50%">FeedBack</th>
                        <th class="bold" width="15%">Client Phone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($follow_up_date as $key => $values) 
                        {
                            $follow_up_date = $values['follow_up_date'];
                            $cro = $values['user_name'];
                            $client_name = $values['client_name'];
                            $feedback = $values['followup_lastfeedback'];
                            $client_phone = $values['client_phone'];
                            ?>
                    <tr>
                        <td><?php echo $follow_up_date; ?></td>
                        <td><?php echo $cro; ?></td>
                        <td><?php echo $client_name; ?></td>
                        <td  ><?php echo $feedback; ?></td>
                        <td><?php echo $client_phone; ?></td>
                    </tr>
                    <?php
                        }
                }
                        
                        ?>
                </tbody>
            </table>

            <?php
                if(isset($meeting))
                {
                    ?>
            <table class="table table-striped table-bordered table-hover" id="Reports2" >
                <thead>
                    <tr>
                        <td colspan="6">
                            <h3 class="bold" style="text-align: center;">Meeting History Report</h3>
                        </td>
                    </tr>
                    <tr>
                        <th class="bold" width="12px">Date</th>
                        <th class="bold" width="10px">Manger</th>
                        <!-- <th class="bold" width="10px">Feedback</th> -->
                        <th class="bold" width="10px">Client Name</th>
                        <th class="bold" width="12px">Client Phone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($meeting as $key => $values) 
                        {
                        
                             $meeting_date = $values['Date'];
                             $manager = $values['user_name'];
                             // $feedback = $values['feedback'];
                             $client_name = $values['client_name'];
                             $client_phone = $values['client_phone'];
                            
                            ?>
                    <tr>
                        <td><?php echo $meeting_date; ?></td>
                        <td><?php echo $manager; ?></td>
                        <!-- <td><?php //echo $feedback; ?></td> -->
                        <td><?php echo $client_name; ?></td>
                        <td><?php echo $client_phone; ?></td>
                    </tr>
                    <?php
                        }
                }     
                        ?> 
                </tbody>
            </table>
        <br>  
        </div>
       <!--  <a href="<?php //echo base_url('Meeting_CI/print_pdf')?>">efs</a> -->
       <button name="create_excel" id="cmd" onclick="run_pdf();" class="btn btn-success" title="download pdf format"><span>Attach Pdf</span> <i class=" glyphicon glyphicon-save"></i></button>
   <button name="create_excel" id="create_excel" class="btn btn-primary" style="margin-left: 15px" title="download excel format"><span>Attach Excel</span> <i class=" glyphicon glyphicon-save"></i></button>
       
        
</div>
<script>
    $(document).ready(function () {
        $('#create_excel').click(function(){
        $("#Reports").table2excel({
            filename: "Reports.xls"
        });
         });
    });
</script>

<!-- <script type="text/javascript">
    function printreport(e1) 
    {
       var restorepage=document.body.innerHTML;
       var printcontent=document.getElementById(e1).innerHTML;
       document.body.innerHTML=printcontent;
       window.print();
       document.body.innerHTML=restorepage;
    }

</script> -->
