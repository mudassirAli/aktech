<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
<!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="portlet">
                <div class="portlet-body form">
                    <h2 class="bold">Reports </h2>
                    <hr>
                    <form role="form" method="post" name="form_data" enctype="multipart/form-data"      action="<?php echo base_url('Reports/report'); ?>">
                        <div class="row" style="width: 60%;box-shadow: 0px 6px 15px #9e9e9e; margin: 0 auto; padding: 30px;">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-body">
                                    <div class="form-group form-md-checkboxes">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="md-checkbox has-success">
                                                    <input type="checkbox" name="cro" id="checkboxcro" value="cro" class="md-check">
                                                    <label for="checkboxcro">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Cro
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="md-checkbox has-success">
                                                    <input type="checkbox" name="manager" id="checkboxmanager" value="" class="md-check">
                                                    <label for="checkboxmanager">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Manager
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="row" >
                                            <div class="col-md-2">
                                <?php
                                             
                                    if(!empty($key))
                                    {     
                                ?>
                                        <div id="croshow" style="display: none">
                                            <select style=" width: 100px;height: 33px;" name="cro_user_id" >
                                <?php
                                            foreach ($key as $value) 
                                            {
                                             $id = $value->user_id;   
                                            $user = $value->user_name;    
                                ?>
                                                <option value="<?php echo $id; ?>"><?php echo $user; ?>
                                                </option>
                                <?php
                                            }
                                ?>
                                            </select>
                                        </div>
                                <?php             
                                    }
                                    else
                                    { 
                                ?>
                                        <h3>No Cro</h3>
                                <?php 
                                    }

                                ?>
                                        </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-md-2">
                                                
                                            
                                            <div <div id="managershow" style="display: none"> 

                                                 <?php
                                             
                                                if(!empty($key1))
                                                {
                                              
                                            ?>
                                                <div>
                                           
                                              
                                                <select style=" width: 100px;height: 33px;" name="meeting_list" >

                                                    <?php
                                                        foreach ($key1 as $value) 
                                                        {   
                                                            $id = $value->user_id;   
                                                            $user = $value->user_name;    
                                                    ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $user; ?>
                                                        </option>
                                                    <?php
                                                          }
                                                    ?>
                                                </select>
                                               
                                            </div>
                                             <?php 
                                                
                                                }
                                                else
                                                { 
                                            ?>
                                                    <h3>No manager</h3>
                                            <?php 
                                                }

                                                ?>
                                            </div>
                                        </div>
                                        </div>
                                        <br>
                                        <br>
                                     <hr>
                                        <div class="md-checkbox-inline">
                                           <div class="row" >
                                                <div class="col-xs-3">
                                                    <div  id="follow" style="display: none;">
                                                    <div class="md-checkbox has-success">
                                                        <input type="checkbox" name="follow_id" id="checkbox2" value="<?php echo $value->user_id;?>" class="md-check">
                                                        <label for="checkbox2">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Follow Up
                                                        </label>
                                                    </div>
                                                </div>
                                                </div>
                                                <!-- <div class="col-xs-3" id="space"></div> -->
                                                <div class="col-xs-1"></div>
                                                 <div class="col-xs-3">
                                                    <div id="meeting" style="display: none;">
                                                    <div class="md-checkbox has-info">
                                                        <input type="checkbox" name="client_meeting" id="checkbox1" value="<?php 
                                                        if(!empty($value->user_id))
                                                        {
                                                            echo $value->user_id;
                                                        }
                                                         ?>" class="md-check">
                                                        <label for="checkbox1">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box">  </span> Meeting  
                                                        </label>
                                                    </div>
                                                </div>
                                                </div>
                                        
                                        
                                            </div>
                                        </div>
                                   
                                <br><br>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <span class="help-block"> Select date range </span>
                                            <div class="input-group input-large date-picker input-daterange"     data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                                <input type="date" class="form-control" name="from">
                                                <span class="input-group-addon"> to </span>
                                                <input type="date" class="form-control" name="to"> 
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-actions noborder">
                                                    <input type="Submit" class="btn blue" value="Submit">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#checkboxcro").click(function () {
            if ($(this).is(":checked")) {
                $("#croshow").show();
                $("#follow").show();
            } else {
                $("#croshow").hide();
                $("#follow").hide();
            }
        });
        
    });
    $(function () {
        $("#checkboxmanager").click(function () {
            if ($(this).is(":checked")) {
                $("#managershow").show();
                $("#meeting").show();
                // $("#space").show();
            } else {
                $("#managershow").hide();
                $("#meeting").hide();
            }
        });
    });
</script>