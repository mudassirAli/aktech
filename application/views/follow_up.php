<style type="text/css">
#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
    <div class="page-content-wrapper" >
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="row">
        <div class="col-md-12">

            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Follow Up -

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('Follow_upC/insert');?>" method="POST" id="followup">
                    <div class="form-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <h3 class="block">Provide your account details</h3>
                                 <?php foreach ($key as $value)
                                    {
                                        $id=$value['user_id'];
                                       ?>
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo $id; ?>"  />
                              <?php }?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Enter Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="name" placeholder="Provide your name" />
                                        <span id="color"><?php echo form_error('name')?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Phone Number
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="Number" class="form-control" name="pno" placeholder="Provide your Phone Number" />
                                        <span id="color"><?php echo form_error('pno')?></span>
                                    </div>
                                </div>

                            <div class="form-group" style="margin-left: 260px">
                 <!--  <label>Select Project type</label><br> -->
                 <!-- <select id="framework" name="framework[]" multiple class="form-control" > -->
                  <div>
                   <input id="a" type="checkbox" name="proposal" required="">
                   <label>Select project type</label>
                 </div>
                 <div id="b" style="display: none;">
                   <input type="radio" name="webmobile" value="Website">
                   <label>web</label>&nbsp
                   <input type="radio"  id="f" name="webmobile" value="Mobile app">
                   <label>mobile app</label>&nbsp
                   <input type="radio"  name="webmobile" id="d" value="Website and">
                   <label>website and mobile app </label>
                 </div>

                 <div id="e" style="display: none;">
                  <input type="radio" name="app" value=" IOS" >
                  <label>ios</label>&nbsp&nbsp
                  <input type="radio" name="app" value=" Andriod" >
                  <label>andriod</label>&nbsp&nbsp
                  <input type="radio" name="app" value=" Hybrid">
                  <label>Hybrid</label>
                </div>

                <div id="g" style="display: none;">
                  <input type="radio" name="webapp" value=" App IOS" >
                  <label>ios</label>&nbsp&nbsp
                  <input type="radio" name="webapp" value=" App Andriod" >
                  <label>andriod</label>&nbsp&nbsp
                  <input type="radio" name="webapp" value=" App Hybrid">
                  <label>Hybrid</label>
                </div>
                <!-- </select> -->
              </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Business
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="business" placeholder="Provide your Business name" />
                                    <span id="color"><?php echo form_error('business')?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Address
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="address" placeholder="Provide your Address" />
                                    <span id="color"><?php echo form_error('address')?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Feedback
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                   <textarea cols="42" rows="3" name="feedback" placeholder="Your Feedback"></textarea>
                                   <span id="color"><?php echo form_error('feedback')?></span>
                               </div>
                           </div>
                           <div class="form-group">
                            <label class="control-label col-md-3">Date
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="date" />
                                <span id="color"><?php echo form_error('date')?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Time
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="time" class="form-control" name="time"  />
                                <span id="color"><?php echo form_error('time')?></span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <a href="<?php echo base_url('Follow_upC/allFollowUp');?>" type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->



<script type="text/javascript">
  $(document).ready(function(){

    $('#a').click(function(){
      $('#b').toggle();
    }); 
    

    $('#f').click(function(){
      $('#e').toggle();
       });
      $('#d').click(function(){
        $('#g').toggle();
      }); 


//    if (!$('#f').click(function(e){e.preventDefault()}))
//    {
//     $('#e').hide();
     
//     } 
//    else
//     {
//      $('#e').show();
//    }
 });

  
</script>



