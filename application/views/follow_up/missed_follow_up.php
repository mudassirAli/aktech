 
                  <!-- datatables -->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');?>">
  <script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');?>" type="text/javascript"></script>
 
                    <div class="row">
                        <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                      <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                      <span>
                                        <i class="fa fa-mobile"></i>
                                        </span>
                                        <span style="margin-left: 7px"> Missed follow Up</span>
                                      </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-hover table-bordered" id="mytable">
                                            <thead>
                                                <tr>
                                                    <th class="bold"> Name </th>
                                                    <th class="bold"> Phone </th>
                                                    <th class="bold"> Business </th>
                                                    <th class="bold"> Address </th>
                                                    <th class="bold"> Feedback </th>
                                                    <th class="bold"> Date </th>
                                                    <th class="bold"> Time</th>
                                                   <th class="bold"> Meeting </th>
                                                    <th class="bold">All Feedback </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>                   
                         </div>
              
                    <!-- END PAGE BASE CONTENT -->
            
                <!-- END CONTENT BODY -->
                <!-- datatabele script -->
       
         
        <script type="text/javascript">
            $(document).ready(function() {
                $('#mytable').DataTable( {
                    processing: true,
                    serverSide: true,
                    Length:10,
                    lengthMenu:[[10,25,50,0],[10,25,50,'All']],
                    ajax: {
                        url: "<?php echo base_url('Missed_Follow_up/missed_follow_up_datatable'); ?>",
                        method:"GET"
                    },
                    "columns": [
                       {"data" : "client_name"},
                        {"data" : "client_phone"},
                        {"data" : "client_business"},
                        {"data" : "client_address"},
                        {"data" : "followup_lastfeedback"},
                        {"data" : "follow_up_date"},
                        {"data" : "follow_up_time"},
                        { "data": "followup_id",render:function(id, type, row){
                         return  '<a href="<?php echo base_url('Meeting_CI/assign_meeting/') ?>'+row.followup_id+'" type="button" name="update" id="'+row.followup_id+'" class="btn btn-success btn-sm"><i class="fa fa-group fa-2x"></i></a>';
                        } },
                       
                        { "data": "followup_id",render:function(id, type, row){
                         return  '<a href="<?php echo base_url('FeedbackC/show_feedback/') ?>'+row.followup_id+'" type="button" name="update" id="'+row.followup_id+'" class="btn btn-primary btn-sm"><i class="fa fa-commenting fa-2x"></i></a>';
                        } }
                    ],
                    columnDefs:[
                        {'targets':7,orderable:false},
                        {'targets':8,orderable:false},
                    ]
         
                });
            });

        </script>
        
