<style type="text/css">
#color
{
    color: white;
    background-color: green;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <?php if ($this->session->flashdata('success')==true) { ?>
           <div class="alert " id="color" >
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            <?php }?>
        </div>
        <div class="col-md-12">
         <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class=""></i>Feedback Table </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">

                        <table class="table table-striped table-hover" id="addfeedback">
                            <thead>
                                <tr>
                                    <th>Feedback-date</th>
                                    <th> Client name </th>
                                    <th> Feedback </th>
                                    
                                    <th> Time </th>
                                    <th> Created_at </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($a as $value) 
                                {
                                    $id=$value['followup_id'];
                                    ?>
                                    <tr>
                                        <td><?php echo $value['feedback_date']?></td>
                                        <td><?php echo $value['client_name']?></td>
                                        <!-- <td><?php //echo $value['feedback']?></td> -->
                                        <td style="text-align: center"><button type="button" class="btn btn-info" onclick="open_modal('<?php echo $value['feedback']; ?>');">  <i class='fa fa-commenting fa-2x'></i></button></td>

                                        <td><?php echo $value['feedback_time']?></td>
                                        <td><?php echo $value['created_at']?></td>
                                        <td>
                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                            <!-- <a class="label label-sm label-danger"> Delete </a> -->
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <a href="<?php echo base_url('FeedbackC/fetch_feeback_id/').$id;?>" class="label label-sm label-success"> Add Feedback <i class="fa fa-plus"></i> </a>
                        <a href="<?php echo base_url('follow_upC/allFollowUp/').$id;?>" class="label label-sm label-danger"> Back </a>
                    </div>
                </div> 
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>



                                    <div id="myModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">Client Feedback</h3>
                                        </div>
                                        <div class="modal-body" >
                                            <ul>
                                                <li id="feedback1"></li>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<script type="text/javascript">
    function open_modal(feedback)
    {
        $('#feedback1').html(feedback);
        $('#myModal').modal('show');
    }

</script>


