<script src="<?php echo base_url()?>assets/DataTables/datatables.bootstrap.js"></script>
<style type="text/css">
#color
{
    color: white;
}
.alert-success {
    background-color: #337d46!important;
    border-color: #337d46!important;
    color: #27a4b0;
}
</style>   
   
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <div class="btn-group">
                                            <?php 
                                            if($this->session->userdata('role')=='cro'){
                                                ?>

                                            <a href="<?php echo base_url('follow_upC/index')?>" id="sample_editable_1_new" class="btn green"> Add New
                                                <i class="fa fa-plus"></i>
                                            <?php }else
                                            {?>
                                            <div class="caption">
                                                <h2 class="bold"><span>
                                                <i class="fa fa-mobile"></i>
                                                </span>
                                                <span style="margin-left: 7px"> All follow Up</span></h2>
                                            </div>
                                            <?php
                                            } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>

                            <?php if ($this->session->flashdata('success')==true) { ?>
                            <div class="alert alert-success" id="color" >
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('unsuccess')==true) { ?>
                            <div class="alert alert-success" id="color" >
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('flash_m')==true) { ?>
                            <div class="alert alert-success" id="color" >
                                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <?php } ?>
                            <table class="table table-striped table-hover table-bordered" id="follow">
                                <thead>
                                    <tr>
                                        <th class="bold"> user id </th>
                                        <th class="bold"> Name </th>
                                        <th class="bold"> Phone # </th>
                                        <th class="bold"> Business </th>
                                        <th class="bold"> Address </th>
                                        <th class="bold"> Application Type</th>
                                        <th class="bold"> Feedback </th>
                                        <th class="bold"> Date </th>
                                        <th class="bold"> Time </th>
                                        <th class="bold"> Meeting </th>
                                        <th class="bold"> Feedback </th>
                                        <th class="bold"> Edit </th>
                                        <th class="bold"> Delete </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
    <!-- END QUICK SIDEBAR -->
<!-- END CONTAINER -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#follow').dataTable( {
                processing: true,
                serverSide: true,
                Length:10,
                order : [[0, 'desc' ]],
                lengthMenu:[[10,25,50,0],[10,25,50,'All']],
                ajax: {
                    url: "<?php echo base_url('Follow_upC/datatable'); ?>",
                    method:"GET"
                },
                "columns": [

                {"data" : "followup_id"},
                {"data" : "client_name"},
                {"data" : "client_phone"},
                {"data" : "client_business"},
                {"data" : "client_address"},
                {"data" : "application_type"},
                {"data" : "followup_lastfeedback"},
                {"data" : "follow_up_date"},
                {"data" : "follow_up_time"},

                { "data": "followup_id",render:function(id, type, row){
                 return  '<a href="<?php echo base_url('Meeting_CI/assign_meeting/') ?>'+row.followup_id+'" type="button" name="update" id="'+row.followup_id+'" class="btn btn-success btn-sm"><i class="fa fa-group fa-2x"></i></a>';
                } },

             { "data": "followup_id",render:function(id, type, row){
                         return  '<a href="<?php echo base_url('FeedbackC/show_feedback/') ?>'+row.followup_id+'" type="button" name="update" id="'+row.followup_id+'" class="btn btn-primary btn-sm"><i class="fa fa-commenting fa-2x"></i></a>';
            } },


             { "data": "followup_id",render:function(id, type, row){
                 return  '<a href="<?php echo base_url('Follow_upC/edit/') ?>'+row.followup_id+'" type="button" name="update" id="'+row.followup_id+'" class="btn btn-primary btn-x8"><span class="glyphicon glyphicon-pencil"></span></a>';
             } },

             { "data": "followup_id",render:function(id, type, row){
                 return  '<a href="<?php echo base_url('Follow_upC/delete/') ?>'+row.followup_id+'" type="button" name="delete" id="'+row.followup_id+'" class="btn btn-danger btn-3x"><span class="glyphicon glyphicon-trash"></span></a>';
             } }
             ],
             columnDefs:[
            {'targets':0,visible:false},
            
            {'targets':9,orderable:false},
            {'targets':10,orderable:false},
            {'targets':11,orderable:false},
            {'targets':12,orderable:false},
             
             ]
         } );
        } );
    </script>




