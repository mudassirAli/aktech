<style type="text/css">
    .error{color: red};
</style>
           
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                       
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                           
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                 
                    <div class="row">
                        <div class="col-md-12">
                           
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase"> Update
                                            
                                        </span>
                                    </div>
                                
                                </div>
                                    <form class="form-horizontal" action="<?php echo base_url('Meeting_CI/update_meeting_record');?>" method="POST">
                                        
                                            <div class="form-body">
                                               
                                                
                                                <div class="tab-content">
                                                    
                                                    <?php
                                                    foreach ($key as $value) 
                                                    {
                                                       ?>
                                                    <input type="hidden" name="id" 
                                                    value="<?php echo $value->metting_id?>"/>
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Provide your account details</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Client name
                                                                
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="cname" 
                                                                value="<?php echo $value->client_name?>

                                                                " placeholder="Provide your First name" />
                                                                <span class="error"><?php echo form_error('cname')?></span>
                                                               
                                                            </div>
                                                        </div><br>
                                                      
                                                      
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Business
                                                               
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" 
                                                                value="<?php echo $value->client_businees?>"" class="form-control" name="business" placeholder="Provide your Business name" />
                                                               <span class="error"><?php echo form_error('business')?></span>
                                                            </div>
                                                        </div><br>
                                                        
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Designation
                                                                
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="designation" 
                                                                value="<?php echo $value->meeting_designation?>" placeholder="Provide your Business name" />
                                                                <span class="error"><?php echo form_error('designation')?></span>
                                                            </div>
                                                        </div><br>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Address
                                                                
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="address" 
                                                                value="<?php echo $value->client_meeting_location?>" placeholder="Provide your Address" />
                                                               <span class="error"><?php echo form_error('address')?></span>
                                                            </div>
                                                        </div><br>
                                                          <div class="form-group">
                                                            <label class="control-label col-md-3">Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="Phone" class="form-control" name="phone" 
                                                                value="<?php echo $value->client_phone?>" placeholder="Provide your Phone Number" />
                                                                <span class="error"><?php echo form_error('phone')?></span>
                                                            </div>
                                                        </div><br>
                                                       
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date
                                                                
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="date" class="form-control" name="date" value="<?php echo $value->client_meeting_date?>" />
                                                               <span class="error"><?php echo form_error('date')?></span>
                                                            </div>
                                                        </div><br>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Time
                                                               
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="time" class="form-control" name="time" value="<?php echo $value->client_name?>  />
                                                                <span class="error"><?php echo form_error('time')?></span>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Feedback
                                                               
                                                            </label>
                                                            <div class="col-md-4">
                                                               <textarea cols="42" rows="3" name="feedback"  placeholder="Your Feedback"><?php echo $value->client_meeting_feedback?></textarea>
                                                                <span class="error"><?php echo form_error('feedback')?></span>
                                                            </div>
                                                        </div><br>

                                                          <input type="hidden" name="role" value="Cro">

                                               </div>
                                                </div>
                                                <input type="submit" name="" class="btn btn-success pull-right" style="text-align: center;">
                                                <?php
                                                }
                                                    ?>
                                            </div>

                                        </form>                                
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
          



