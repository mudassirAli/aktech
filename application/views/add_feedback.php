
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Feedback Insert

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('FeedbackC/insert_feedback');?>" method="POST">
                    <div class="form-body">
                        <div class="tab-content">
                            
                            <div class="tab-pane active" >
                                <h3>Provide Feedback</h3>

                                <?php foreach ($b as $value) 
                                              {
                                                  $id=$value['followup_id'];
                                               ?>
                                              <input type="hidden" name="id" value="<?php echo $id; ?>">
                                              <?php }?>
                                              
                                <div class="form-group">
                                    <label class="control-label col-md-3">Feedback
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                     <textarea cols="42" rows="3" required name="feedback" placeholder="Your Feedback"></textarea>
                                 </div>
                             </div>
                               <div class="form-group">
                                    <label class="control-label col-md-3">Next Follow Up Date
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                    <input type="Date" name="date" required="">
                                 </div>
                             </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Time
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                     <input type="time" name="time" required="">
                                 </div>
                             </div>
                              
                             
                             <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-circle green">Submit</button>
                                        <a  type="button" class="btn btn-circle grey-salsa btn-outline" href="<?php echo base_url('Follow_upC/allFollowUp');?>">Cancel</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </form>                                
            </div>
        </div>
    </div>

    <!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





