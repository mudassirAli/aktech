<!-- <style type="text/css">
#color
{
    color: white;
    background-color: green;
}
</style> -->
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <?php if ($this->session->flashdata('success')==true) { ?>
           <div class="alert " id="color" >
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            <?php }?>
        </div>
        <div class="col-md-12">
         <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class=""></i>Feedback Table </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover" id="addfeedback">
                            <thead>
                                <tr>
                                    <th>Feedback-date-time</th>
                                    <th> Client name </th>
                                    <th> Show Feedback </th>
                                    <th>Manager</th>
                                    <th> Next Meeting Date </th>
                                    <th> Time </th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($key as $value) 
                              {
                                  
                                  ?>
                                  <tr>
                                    <td><?php echo $value['created_at']?></td>
                                    <td><?php echo $value['client_name']?></td>
                                    <!--  <td><?php //echo $value['feedback']?></td> -->
                                    <td style="text-align: center"><button type="button" class="btn btn-info " onclick="open_modal('<?php echo $value['feedback']; ?>');">  <i class='fa fa-commenting fa-2x'></i></button></td>

                            
                            <td><?php echo $value['user_name']?></td>
                            <td><?php echo $value['feedback_date']?></td>
                            <td><?php echo $value['feedback_time']?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                      <!--   <div class="col-md-2" style="margin-top: 12px">
                            <div class="btn-group">
                                <a href="<?php //echo base_url('Meeting_CI/new_meeting/').$id;?>" id="sample_editable_1_new" class="btn btn-success">New Meeting
                                    <i class="glyphicon glyphicon-plus"></i></a>

                                </div>
                            </div> -->
                            <div style="margin-left:800px">                           
                              <div class="col-md-7" style="margin-top: 12px">
                                <div class="btn-group">
                                    <a href="<?php echo base_url('Meeting_CI/show_feedback_form/').$key2;?>" id="sample_editable_1_new" class="btn btn-primary">New feedback
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 12px">
                                <div class="btn-group">
                                    <a href="<?php echo base_url('Meeting_CI/today_meeting/').$key2;?>" id="sample_editable_1_new" class="btn btn-success">Back
                                        <i class="glyphicon glyphicon-backward"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>


                                    <div id="myModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">Client Feedback</h3>
                                        </div>
                                        <div class="modal-body" id="feedback1">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<script type="text/javascript">
    function open_modal(feedback)
    {
        $('#feedback1').html(feedback);
        $('#myModal').modal('show');
    }

</script>

<!-- <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.11.4/themes/smoothnes/jquery-ui.css">
<script type="text/javascript" src="cordva.js"></script>
<script type="text/javascript" src="code.jquery.com/jquery-1.10.2.js"></script> -->

<!-- <form id="window" style="height: 50%;width: 50%;">
    <p>hello this is testing</p>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#feedback_show').click(function(){
            alert();  
        });
    });
</script> -->






