
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
  <!-- BEGIN PAGE HEAD-->
  <div class="page-head">
    <div class="page-toolbar">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="portlet light bordered" id="form_wizard_1">
        <div class="portlet-title">
          <div class="caption">
            <i class=" icon-layers font-red"></i>
            <span class="caption-subject font-red bold uppercase"> Feedback Insert
            </span>
          </div>                               
        </div>
        <form class="form-horizontal" action="<?php echo base_url('Meeting_CI/insert_feedback');?>" method="POST">
          <div class="form-body">
            <div class="tab-content">

              <div class="tab-pane active" >
                <h3>Provide Feedback</h3>

                <?php
                $id=$key;
                ?>
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <div class="form-group">
                  <div class="row">
                    <label class="control-label col-md-3">Feedback
                      <span class="required"> * </span>
                    </label>
                    <div class="col-md-4">
                     <textarea cols="42" rows="3" required name="feedback" placeholder="Your Feedback"></textarea>
                   </div>
                 </div>
                 <br> 
                 <div class="form-group">
                  <div class="row">
                    <label class="control-label col-md-3">Manager
                      <span class="required"> * </span>
                    </label>
                    <div class="col-md-4">
                     <select style=" width: 327px;height: 33px;" name="list" >
                      <?php
                      foreach ($key2 as $value2) 
                      {
                        ?>
                        <option value="<?php echo $value2->user_id ?>"><?php echo $value2->user_name ?>
                        </option>
                        <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <br>
              <div class="form-group form-md-checkboxes">
                <div class="row">
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-3" id="open_meeting_view">
                   <div class="md-checkbox-inline">
                    <div class="md-checkbox has-success">
                      <input type="checkbox" id="checkbox1"  class="md-check">
                      <label for="checkbox1">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span> Next Meeting
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div id="add_meeting" style="display: none;">
              <div class="form-group" >
                <div class="row" >
                  <label class="control-label col-md-3">Next Meeting Date
                    <span> * </span>
                  </label>
                  <div class="col-md-4">
                    <input type="Date" name="date">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="control-label col-md-3">Time
                    <span>* </span>
                  </label>
                  <div class="col-md-4">
                   <input type="time" name="time">
                 </div>
               </div>
             </div>
           </div>
           <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-circle green">Submit</button>
                <a  type="button" class="btn btn-circle grey-salsa btn-outline" href="<?php echo base_url('Meeting_CI/today_meeting');?>">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>                                
</div>
</div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->
<script type="text/javascript">
  $(document).ready(function(){
        // e.preventDefault();
       $('#checkbox1').click(function(){
        if ($(this).is(":checked")){
          $('#add_meeting').show();
        }
        else
        {
           $('#add_meeting').hide();
        }
        
        });
      });
    </script>





