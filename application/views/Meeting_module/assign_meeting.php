<style type="text/css">
.error{color: red};
</style>
<script src="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.css">

<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        
        <!-- END PAGE TITLE -->
        <!-- BEGIN PAGE TOOLBAR -->
        <div class="page-toolbar">
            <!-- BEGIN THEME PANEL -->
            
            <!-- END THEME PANEL -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
    
    <div class="row">
        <div class="col-md-12">
         
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Meeting
                            
                        </span>
                    </div>
                    
                </div>
                
                <form class="form-horizontal" action="<?php echo base_url('Meeting_CI/client_meeting');?>" method="POST">
                    
                    <div class="form-body">
                     
                     

                        <div class="tab-content">
                            
                            <div class="tab-pane active" id="tab1">
                                <h3 class="block">Provide your account details</h3>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Client Name
                                     
                                    </label>
                                    <div class="col-md-4">
                                     
                                        <input type="text" class="form-control" name="business" placeholder="Provide your Business name" 
                                        value="<?php echo $key->client_name?>" readonly />
                                        <input type="hidden" name="follow_id" 
                                        value="<?php echo $key->followup_id?>">
                                        
                                    </div>
                                </div><br> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Applicaton-types
                                     
                                    </label>
                                    <div class="col-md-4">
                                     
                                        <input type="text" class="form-control" name="business" placeholder="Provide your Business name" 
                                        value="<?php echo $key->application_type?>" readonly />
                                        
                                        
                                    </div>
                                </div><br> 


                                                         
                            
                            <div class="form-group">
                                <label class="control-label col-md-3">Manager
                                  
                                </label>
                                <div class="col-md-4">

                                     <select style=" width: 320px;height: 33px;" name="list" >
                                                    <?php
                                                        foreach ($key2 as $value2) 
                                                        {
                                                            
                                                    ?>
                                                        <option value="<?php echo $value2->user_id ?>"><?php echo $value2->user_name ?>
                                                        </option>
                                                    <?php
                                                         }
                                                    ?>
                                                              </select>
                                           
                                                            </div>
                                                           
                                                        </div><br>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">Date
                                     
                                    </label>
                                    <div class="col-md-4">
                                     
                                        <input type="date" class="form-control" name="date">
                                        
                                    </div>
                                </div><br> 
                                   <div class="form-group">
                                    <label class="control-label col-md-3">Time
                                     
                                    </label>
                                    <div class="col-md-4">
                                     
                                        <input type="time" class="form-control" name="time">
                                        
                                    </div>
                                    
                                     
                                </div>
                                 <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-success pull-right" style="margin-right: 20px">
                                     </div>                                
                   
                </div>

            </form>                                
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>





