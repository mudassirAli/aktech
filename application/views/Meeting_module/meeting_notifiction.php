<!DOCTYPE html>
<html>
 <head>
  <title>Webslesson Tutorial | Facebook Style Header Notification using PHP Ajax Bootstrap</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <br /><br />
  <div class="container">
   <nav class="navbar navbar-inverse">
    <div class="container-fluid">
     <div class="navbar-header">
      <a class="navbar-brand" href="#">Webslesson Tutorial</a>
     </div>
     <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
       <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-envelope" style="font-size:18px;"></span></a>
       <ul class="dropdown-menu"></ul>
      </li>
     </ul>
    </div>
   </nav>
   <br />
   <h2 align="center">Facebook Style Header Notification using PHP Ajax Bootstrap</h2>
   <br />
   <form method="post" id="comment_form">
    <div class="form-group">
     <label>Enter Subject</label>
     <input type="text" name="subject" id="subject" class="form-control">
    </div>
    <div class="form-group">
     <label>Enter Comment</label>
     <textarea name="comment" id="comment" class="form-control" rows="5"></textarea>
    </div>
    <div class="form-group">
     <input type="submit" name="post" id="post" class="btn btn-info" value="Post" />
    </div>
   </form>
   
  </div>
 </body>
</html>

<script>
$(document).ready(function(){
function load_unseen_notification(view = '')
 {
  $.ajax({
   url:'<?php echo base_url();?>Meeting_CI/view_noification',
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    console.log(data);return false;
    if(data.unseen_notification > 0)
    {
      $(".dropdown-menu").html(data.unseen_notification);
     $('.count').html(data.unseen_notification);
         var text = '';
        data.notification.forEach(function(val,index)
        {
         var comment_subject = val.comment_subject.substring(0, 10);
         var comment_text = val.comment_text.substring(0, 20);

  text +='<li><strong>'+comment_subject+'</strong><br /><small><em>'+comment_text+'</em></small></li>';
          $(".dropdown-menu").html(text);
           });
        }
                  //<!--End if-->
        else
 {
       text += '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
       $(".dropdown-menu").html(text);
 }
                  //<!--End if else false-->
        }
     });
}
 load_unseen_notification();
 $(document).on('click', '.dropdown-toggle', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });
 setInterval(function()
 { 
  load_unseen_notification(); 
 }, 5000);
});
</script>