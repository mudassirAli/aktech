<style type="text/css">
    
    #color
    {
        color: red;
    }
</style>
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <div class="page-toolbar">
                        </div>
                    </div>
                 
                    <div class="row">
                        <div class="col-md-12">
                           
                            <div class="portlet light bordered" id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Update Follow Up -
                                            
                                        </span>
                                    </div>                               
                                </div>
                                    <form class="form-horizontal" action="<?php echo base_url('follow_upC/update');?>" method="POST" id="followup">
                                        <div class="form-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1">
                                                        <h3 class="block">Provide your account details</h3>

                                                        <div class="form-group">
                                                            
                                                     <label class="control-label col-md-3">Enter Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <?php
                                                            foreach ($a as $value) 
                                                            {
                                                                $id = $value['followup_id'];
                                                             ?>
                                                            <input type="hidden" name="id" value="<?php echo $id; ?>" >
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="name" placeholder="Provide your name" value="<?php echo $value['client_name'];?>" />
                                                                <span id="color"><?php echo form_error('name')?></span>
                                                           </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Phone Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="Number" class="form-control" name="pno" placeholder="Provide your Phone Number" value="<?php echo $value['client_phone'];?>"  required />
                                                                <span id="color"><?php echo form_error('pno')?></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Business
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" value="<?php echo $value['client_business'];?>" class="form-control" name="business" placeholder="Provide your Business name" />
                                                                <span id="color"><?php echo form_error('business')?></span>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Address
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="address" placeholder="Provide your Address" value="<?php echo $value['client_address'];?>"/>
                                                                <span id="color"><?php echo form_error('address')?></span>
                                                            </div>
                                                        </div>
                                                 
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="date" class="form-control" name="date" value="<?php echo $value['follow_up_date'];?>" />
                                                                <span id="color"><?php echo form_error('date')?></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Time
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="time" class="form-control" name="time" value="<?php echo strtotime($value['follow_up_time']);?>"/>
                                                                <span id="color"><?php echo form_error('time')?></span>
                                                            </div>
                                                        </div>
                                                         <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                               
                                                    <?php }?>

                                               </div>
                                                </div>
                                            </div>
                                        </form>                                
                            </div>
                        </div>
                    </div>
                    
                    <!-- END PAGE BASE CONTENT -->
                </div>
                
          

                <!-- END CONTENT BODY -->
            
     
        


