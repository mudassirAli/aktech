<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="<?php echo base_url('assets/script');?>" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ULTIMO Admin Dashboard Template</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" >
<script src="<?php echo base_url()?>assets/asets/js/jquery-2.1.0.js"></script> 
<script src="<?php echo base_url()?>assets/asets/js/bootstrap.min.js"></script>
<link href="<?php echo base_url()?>assets/asets/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/asets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/asets/css/animate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/asets/css/admin.css" rel="stylesheet" type="text/css" />
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="login_page">
  <div class="login_content" style="width: 28%!important; padding: 12px!important;">
  <div class="panel-heading border login_heading">sign in now</div> 
  <?php echo validation_errors('<div class="text-danger" style="font-size: 15px">','</div>')?>

  <?php
 if($this->session->flashdata('message'))
 {
  ?>
  <div class="text-danger" style="font-size: 15px">
  <strong>Alert:</strong><?php echo $this->session->flashdata('message')?>
  </div>
  <?php 
 }
  ?>
 <form  id="Sign_up" method="post" action="<?php echo base_url('LoginCI/login_data')?>">

      <div class="form-group">
        
        <div class="col-sm-7">
          <td><input type="email" name="email"  placeholder="Email"  class="form-control" id="email_error"></td>   
        </div>
        <div class="col-sm-5"><td><span class="error_form" id="email_error_message" style="color:red;    font-weight: 500;"></span></td></div>
      </div>
      <div class="form-group">
        <div class="col-sm-7">
          <i class="fas fa-eye" id="eye" onclick="show()" style="position:relative;left:80px;top: 22px;font-size: 16px   "></i>
          <input type="password" name="password" placeholder="Password" id="password" class="form-control">
        </div>

         <div class="col-sm-5">
          <br>

          <td>
            <span class="error_form" id="password_error_message" style="color:red;    font-weight: 500;">          </span>
          </td>
        </div>
      </div>
      <div class="form-group" >
        <div class="col-sm-8">
          <div class="checkbox checkbox_margin" style="padding-left: 0px!important">
            <label style="font-size: 13px;margin-right:200px;margin-top:10px;color: #091282; font-weight: bold; ">Status</label><br>
         
            <input type="radio" name="gender" value="admin" id="radio_value"> <span style="margin-right: 10px;">  Admin </span>

            <input type="radio" name="gender" value="manager" id="radio_value"><span style="margin-right: 10px;">  Manager</span> 
             <input type="radio" name="gender" value="cro" id="radio_value"> <span style="margin-right: 10px;">  Cro</span>
          </div>
        </div>
        <div class="col-sm-4">
          <span class="error_form" id="password_error_message" style="color:red;    font-weight: 500;">  
        </div>
    </div>
      <div class="form-group">
        
        <div class=" col-sm-7">
            <label class="lable_margin">
              <input type="checkbox"><p class="pull-right"> Remember me</p></label>
              <a href="index.html">
              <button class="btn btn-default pull-right" type="submit" >Sign in</button>
              </a>
        </div>
        <div class="col-sm-5"></div>
      </div>
      <div><a href="<?php echo base_url('LoginCI/forgetpassword_load')?>" style="text-decoration: none;margin-left: 150px">Forget Password</div>
        
    </form>
  
 </div>
  </div>
</div>
  

</body>
</html>
<script type="text/javascript">

    $('#email_error').keyup(function(){  
           var email = $('#email_error').val();
           if(email != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url(); ?>LoginCI/available_email",  
                     method:"POST",  
                     data:{email:email},  
                     success:function(data){  

                          $('#email_error_message').html(data);  
                     }  
                });  
           }  
      });


  <!--Code of Show password in Login-->


  function show()
  {
  var x=document.getElementById('password');
  if(x.type=='password')
  {

    x.type='text';
    document.getElementById('eye').style.color="blue";
  }
  else
  {
    x.type='password';
    document.getElementById('eye').style.color="black";
  }
}

//<!--Code of Show validation in Login-->


$(document).ready(function()
{
  $(function()
{
  var error_password=false;
  $('#password').on('keyup',function()
{
       show_password_error();
});

  function show_password_error()
  {
    var password_length=$('#password').val().length;
    if(password_length< 8)
    {
      $('#password_error_message').html('at least 8 characters');
      $('#password_error_message').show();
      error_password=true;
    }
    else 
    {
       $('#password_error_message').html('<label class="text-success "><span class="glyphicon glyphicon-ok"></span>Password good</span></label>');
      $('#password_error_message').show();
    }
  }
  
  $("#Sign_up").submit(function() {
       error_password= false;
       show_password_error();
       if(error_password==false)
       {
        return true;
       }
       else
       {
        return false;
       }               

  });
});
});
</script>

