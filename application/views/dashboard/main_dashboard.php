<script src="<?php echo base_url()?>assets/DataTables/datatables.bootstrap.js"></script>
            <!-- BEGIN CONTENT -->
            
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->

                        <div class="page-title">
                            <h1> Dashboard
                                <!-- <small>statistics, charts, recent events and reports</small> -->
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Dashboard</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div class="row">
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 grey-gallery" href="<?php echo base_url('Follow_upC/allFollowUp')?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $allFollowUpCount;?>"><?php echo $allFollowUpCount; ?></span>
                                        <span class="glyphicon glyphicon-signal"></span>
                                        <!-- <i class="glyphicon glyphicon-signal"></i> -->
                                        <!-- <span class="title" style="margin-left: 20px">Done Follow Up</span>  -->
                                    </div>
                                    <div class="desc"> All Follow Up</div>
                                </div>
                            </a>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue-chambray" href="<?php echo base_url('Done_Follow_up')?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $doneFollowUpCount;?>"><?php echo $doneFollowUpCount; ?></span>
                                        <span class="glyphicon glyphicon-thumbs-up"></span>
                                        <!-- <span class="title" style="margin-left: 20px">Done Follow Up</span>  -->
                                    </div>
                                    <div class="desc"> Done Follow Up</div>
                                </div>
                            </a>
                        </div>
                        
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url('Today_Follow_up');?>">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                   
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $todayFollowupCount; ?>"><?php echo $todayFollowupCount;  ?></span>
                                        <i class="fa fa-mobile fa 1px" style="margin-left: 5px"></i>
                                    </div>
                                    <div class="desc"> Today Folow Up</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo base_url('Missed_Follow_up');?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $missedFollowUpCount;?>"><?php echo $missedFollowUpCount; ?></span>
                                         <i class="fa fa-phone fa 1px" style="margin-left: 5px"></i>

                                    </div>
                                    <div class="desc"> Missed Folow Up </div>
                                </div>
                            </a>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" 
                            href="<?php echo base_url('Meeting_CI/missed_meeting')?>">
                                <div class="visual">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" 
                                        data-value="<?php echo $missedmeeting?>">0</span><i class="fa fa-group fa 1px" style="margin-left: 20px"></i></div>
                                    <div class="desc"> Missed meeting </div>
                                </div>
                            </a>
                        </div>
                       
                       
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url('Meeting_CI/today_meeting')?>">
                                <div class="visual">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo $todaymeeting?>">0</span>
                                        <i class="fa fa-group" style="margin-left: 20px"></i>
                                    </div>
                                    <div class="desc"> Today meeting</div>
                                </div>
                            </a>
                        </div>
                    </div>
                   <script src="<?php echo base_url()?>assets/DataTables/datatables.bootstrap.js"></script>

  
<script src="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/Swetalerts/sweetalert2.min.css">
<script src="<?php echo base_url()?>assets/toaster/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/toaster/toastr.min.css">
     <?php
if($this->session->flashdata('msg')==true)
{

    ?>
    <script type="text/javascript">
        
      toastr.success('Data Updated SuccessFully');
    </script>      
    <?php }?>

        <?php
if($this->session->flashdata('msg2')==true)
{

    ?>
    <script type="text/javascript">
        
      swal({
  position: 'top-end',
  type: 'success',
  title: 'Your  Data has been Inserted',
  showConfirmButton: false,
  timer: 1500
});
    </script>      
    <?php }?>

      
                  
                  
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        
                            <!-- END EXAMPLE TABLE PORTLET-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                      <span>
                                        <i class="fa fa-group"></i>
                                        </span>
                                        <span style="margin-left: 7px"> Today Meeting Records</span>
                                      </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="user_data">
                                        <thead>
                                            <tr>
                                              <th width="15%"> Client_Name </th>
                                              <th width="14%">Client_businees </th>
                                              <th width="18%">Applicaton-types</th>
                                              <th width="8%">Location </th>
                                              <th width="11%">client_phone </th>
                                              <th width="11%">Manager </th>
                                              <th>Date</th>
                                              <th>Time </th> 
                                               <th>Feedback</th> 
                                               <th>Proposal</th>
                                              <th>Delete</th>                                           
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->

       
        <script type="text/javascript" language="javascript">
    $(document).ready(function(){

        var datatable=$('#user_data').DataTable({
            bProcessing: true,
            bServerSide: true,
            Length:10,
            lengthMenu:[[10,25,50,0],[10,25,50,'All']],
            ajax: {
              url: "<?php echo base_url('Meeting_CI/today_meeting_datatables'); ?>",
              method:"GET"
            },  
            "columns": 
            [
            {"data" : "client_name"},
            {"data" : "client_business"},
            {"data" : "application_type"},
            {"data" : "client_address"},
            {"data" : "client_phone"},
            {"data" : "user_name"},
            {"data" : "Date"},
            {"data" : "Time"},
            {"data" :"metting_id",render:function(id,type,row){
                  return "<a href='<?php echo base_url('Meeting_CI/show_all_feedback/')?>"+row.metting_id+"' class='btn btn-sm'><i class='fa fa-commenting-o fa-3x'></i></a>"
            }}, 
             // {"data": "metting_id",render:function(id, type, row){
             //     return  '<a href="<?php //echo base_url('Meeting_CI/get_data_for_update/') ?>'+row.metting_id+'" data-idvalue = '+row.metting_id+' class="btn btn-sm btn-success"><i class="fa fa-pencil fa-1x" ></i></a>';
             {"data" :"metting_id", render:function(id,type,row){
              return "<a href='<?php echo base_url('ProposalC/fetch_feeback_id/')?>"+row.followup_id+"' class='btn btn-sm btn-success'><i class='fa fa-dollar fa-2x'></a>";
            }},
             //    }},
                {"data": "metting_id",render:function(id, type, row){
                 return  "<button class='btn btn-sm btn-danger' id='delete' data-idvalue = '"+row.metting_id+"' data-toggle='tooltip' title='Delete Record!' ><i class='fa fa-trash fa-2x'></i></button>";
            }}
           ],
           columnDefs:
           [
                {'targets':8,orderable:false },
                {'targets':9,orderable:false },
                {'targets':10,orderable:false },
                
           ]

        });
      });
     
         </script>

<script>
  $(document).ready(function(){
  
    $(document).on('click', '#delete', function(e){
      e.preventDefault();
      var id = $(this).data('idvalue');
      var value = $(this);  
      SwalDelete2(id,value);
    });
    
  });

    function SwalDelete2(id,value){

    swal({
     title: 'Are you sure?',
      text: "It will be deleted permanently!",
      type: 'warning',//
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ok',
      showLoaderOnConfirm: true,
        
      preConfirm: function() {
        return new Promise(function(resolve) {
         $.ajax({
          method:'POST',
          url:"<?php echo base_url('Meeting_CI/delete_meeting_record');?>",
          dataType:'json',
          data:"delete="+id,
          })
           .done(function(response){
            swal('Deleted...',response.message,"D");
           removeTR2(value);
           })
           .fail(function(){
            swal('oops...', "Something Went Wrong","error");
           });
        });
        },
      allowOutsideClick: false        
    }); 
    
  }
  function removeTR2(value){
     $(value).closest('tr').remove();
  }

</script>
