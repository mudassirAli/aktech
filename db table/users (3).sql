-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2018 at 09:24 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_phone`, `user_address`, `user_email`, `user_password`, `role`, `image`, `date`, `created_at`, `updated_at`, `code`) VALUES
(1, 'Umair', '0332323234', 'ichra', 'manager2@gmail.com', '123456789', 'manager', 'asd7.jpg', '2018-08-30', '2018-08-30 12:57:11', '2018-09-11 11:28:12', ''),
(3, 'ALI', '03314394966', 'Ichra', 'cro@gmail.com', '123456789', 'cro', 'ali12.jpg', '2018-08-30', '2018-08-29 19:00:00', '2018-09-12 12:26:11', ''),
(4, 'waqas', '32242422552', 'ichra', 'cro2@gmail.com', '123456789', 'cro', 'asd7.jpg', '2018-08-30', '2018-08-29 19:00:00', '2018-09-10 07:39:43', ''),
(5, 'Kashif', '03314343354', 'Rehamn Pura', 'manager@gmail.com', '123456789', 'manager', '27971783_587178448300836_4514984622663465072_n7.jpg', '2018-08-30', '2018-08-29 19:00:00', '2018-09-11 11:27:45', ''),
(6, 'ALI RAZA', '03314394966', 'ichra', 'admin@gmail.com', '123456789', 'admin', 'ali2.jpg', '2018-08-30', '2018-08-29 19:00:00', '2018-09-11 10:11:30', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
