-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2018 at 03:21 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `proposals`
--

CREATE TABLE `proposals` (
  `proposal_id` int(11) NOT NULL,
  `followup_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_type` varchar(255) NOT NULL,
  `project_deadline` varchar(255) NOT NULL,
  `project_price` varchar(255) NOT NULL,
  `project_description` varchar(255) NOT NULL,
  `proposal_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proposals`
--

INSERT INTO `proposals` (`proposal_id`, `followup_id`, `user_id`, `project_name`, `project_type`, `project_deadline`, `project_price`, `project_description`, `proposal_date`, `created_at`, `updated_at`) VALUES
(52, 84, 1, 'Kato Whitaker', 'Mobile Application IOS', 'Autem consequat Sequi et eos elit id quo nihil iste rerum vel sunt Nam', '81', 'Rerum dolore laborum vel sunt maxime perferendis ut non quis id anim et vel quo earum minima aliquip nihil sit', '2018-08-28', '2018-08-28 07:57:06', '2018-08-28 08:11:37'),
(53, 84, 1, 'Joseph Palmer', 'Mobile Application ', 'Eveniet eaque sapiente culpa sed pariatur Rerum consequuntur nesciunt laboris atque possimus quo ullamco quasi adipisicing velit blanditiis', '76', 'Nisi vel vitae mollit do irure pariatur Perspiciatis veniam voluptatem Alias ab non in', '2018-08-28', '2018-08-28 08:01:10', '2018-08-28 08:11:32'),
(59, 84, 0, 'Burke Huffman', 'Website ', 'Amet esse ut dolorum assumenda perspiciatis beatae aperiam', '1', 'Explicabo Id autem qui commodo nobis fuga Consequatur sit blanditiis irure odio labore ea', '2018-08-28', '2018-08-28 09:05:29', '0000-00-00 00:00:00'),
(60, 84, 0, 'marketing', 'Website ', '50 days', '50000', 'marketing', '2018-08-28', '2018-08-28 09:10:47', '0000-00-00 00:00:00'),
(61, 84, 0, 'abc', 'onWebsite And Hybrid', '40 days', '500000', 'dfsafsaasfs', '2018-08-28', '2018-08-28 09:56:51', '0000-00-00 00:00:00'),
(62, 84, 0, 'abc', 'onWebsite And Hybrid', '40 days', '500000', 'dfsafsaasfs', '2018-08-28', '2018-08-28 09:57:29', '0000-00-00 00:00:00'),
(63, 84, 0, 'Keaton Carver', 'Website ', 'Commodo labore qui laborum Omnis aut quo tempora aliquam ut qui', '-9', 'Voluptate in enim harum incidunt accusamus non est illum eum et est pariatur', '2018-08-28', '2018-08-28 10:27:16', '0000-00-00 00:00:00'),
(64, 84, 0, '', '', '', '', '', '2018-08-28', '2018-08-28 10:40:33', '0000-00-00 00:00:00'),
(65, 84, 0, '', 'Website ', '', '', '', '2018-08-28', '2018-08-28 10:48:11', '0000-00-00 00:00:00'),
(66, 84, 0, 'Cadman Short', 'Website ', '20 days', '12', 'asdf', '2018-08-28', '2018-08-28 11:41:27', '0000-00-00 00:00:00'),
(67, 84, 0, 'marketing', 'onWebsite And Hybrid', '50 days', '50000', 'fsjladka;sdflfjksd', '2018-08-28', '2018-08-28 13:22:55', '0000-00-00 00:00:00'),
(68, 113, 0, 'marketing', 'Website ', '50 days', '50000', 'next project', '2018-08-30', '2018-08-30 12:47:18', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `proposals`
--
ALTER TABLE `proposals`
  ADD PRIMARY KEY (`proposal_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `proposals`
--
ALTER TABLE `proposals`
  MODIFY `proposal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
