-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2018 at 06:55 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `follow_up`
--

CREATE TABLE `follow_up` (
  `followup_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_phone` varchar(255) NOT NULL,
  `client_business` varchar(255) NOT NULL,
  `client_address` varchar(255) NOT NULL,
  `application_type` varchar(255) NOT NULL,
  `followup_lastfeedback` text NOT NULL,
  `follow_up_date` date NOT NULL,
  `follow_up_time` time(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow_up`
--

INSERT INTO `follow_up` (`followup_id`, `user_id`, `client_name`, `client_phone`, `client_business`, `client_address`, `application_type`, `followup_lastfeedback`, `follow_up_date`, `follow_up_time`, `created_at`, `updated_at`) VALUES
(7, 3, 'ali', '02323424324', 'Voluptas iure commodo consequatur voluptas minim debitis et rem praesentium at et quas culpa ducimus sapiente est modi occaecat voluptas', 'House no 471 Mohallah pir islam Haveli Lakha District Okara tehsil Depalpur', 'Mobile app Hybrid', 'my l9ive', '2018-09-03', '12:31:00.000000', '2018-09-01 17:19:11', '2018-09-03 09:39:11'),
(8, 3, 'shahnawaz', '02323424324', 'Voluptas iure commodo consequatur voluptas minim debitis et rem praesentium at et quas culpa ducimus sapiente est modi occaecat voluptas', 'House no 471 Mohallah pir islam Haveli Lakha District Okara tehsil Depalpur', 'Mobile app Hybrid', 'dsfsafa', '2018-09-03', '00:00:00.000000', '2018-09-01 17:20:16', '2018-09-03 10:54:19'),
(9, 4, 'Hayes Lopez', '58', 'Quod aliquam dolorem porro aliqua Aliquid dolores incididunt laboris alias', 'Omnis saepe placeat laudantium iste asperiores aut rerum voluptatem eos aut anim quos repellendus Placeat maiores est commodi consequatur', 'Mobile app Andriod', 'qwerty', '2018-09-03', '12:21:00.000000', '2018-09-01 17:24:40', '2018-09-03 11:23:49'),
(10, 4, 'raza', '59', 'Exercitationem pariatur Veritatis consectetur qui', 'House no 471 Mohallah pir islam Haveli Lakha District Okara tehsil Depalpur', 'Mobile app Hybrid', 'today meeting', '2018-09-01', '00:32:00.000000', '2018-09-02 09:11:13', '2018-09-03 11:00:21'),
(11, 3, 'waqas', '02323424324', 'Voluptas iure commodo consequatur voluptas minim debitis et rem praesentium at et quas culpa ducimus sapiente est modi occaecat voluptas', 'House no 471 Mohallah pir islam Haveli Lakha District Okara tehsil Depalpur', 'Mobile app Hybrid', 'ali', '2018-09-03', '12:12:00.000000', '2018-09-03 07:13:42', '2018-09-03 08:21:52'),
(12, 3, 'rehman', '2123', 'xyz', 'sdfsdafsss', 'Website and App Andriod', 'shah', '2018-09-03', '11:12:00.000000', '2018-09-03 09:36:37', '2018-09-03 11:21:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `follow_up`
--
ALTER TABLE `follow_up`
  ADD PRIMARY KEY (`followup_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `follow_up`
--
ALTER TABLE `follow_up`
  MODIFY `followup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
