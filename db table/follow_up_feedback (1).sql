-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2018 at 12:58 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `follow_up_feedback`
--

CREATE TABLE `follow_up_feedback` (
  `feedback_id` int(11) NOT NULL,
  `followup_id` int(11) NOT NULL,
  `feedback` text NOT NULL,
  `feedback_date` date NOT NULL,
  `feedback_time` time(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow_up_feedback`
--

INSERT INTO `follow_up_feedback` (`feedback_id`, `followup_id`, `feedback`, `feedback_date`, `feedback_time`, `created_at`, `updated_at`) VALUES
(109, 79, 'have a high responsibilty', '2018-08-18', '00:00:00.000000', '2018-08-17 15:10:10', '2018-08-19 00:02:50'),
(118, 79, 'asdfsadfsdf', '2018-08-08', '15:04:00.000000', '2018-08-17 15:19:12', '0000-00-00 00:00:00'),
(119, 79, 'sadfasdfsdasddfsasd', '2018-08-06', '03:04:00.000000', '2018-08-17 15:19:36', '0000-00-00 00:00:00'),
(120, 79, 'new', '2018-08-17', '00:22:00.000000', '2018-08-17 15:25:12', '0000-00-00 00:00:00'),
(121, 80, 'Dolor ', '2018-08-11', '00:00:00.000000', '2018-08-17 15:40:06', '2018-08-28 14:25:21'),
(122, 81, 'Sed sequi cillum tenetur in sint eaque consequuntur accusamus ea ex iusto delectus do molestias', '2018-08-10', '00:00:00.000000', '2018-08-17 15:51:46', '2018-08-28 14:25:07'),
(123, 82, 'Vitae ', '2018-08-14', '00:00:00.000000', '2018-08-27 18:46:07', '2018-08-28 14:25:28'),
(124, 80, 'sdgws', '2018-08-28', '14:12:00.000000', '2018-08-28 14:39:19', '0000-00-00 00:00:00'),
(125, 80, 'ALi raza', '2018-08-29', '11:11:00.000000', '2018-08-28 15:38:48', '0000-00-00 00:00:00'),
(126, 80, 'adasd', '2018-08-28', '12:31:00.000000', '2018-08-28 16:38:12', '0000-00-00 00:00:00'),
(127, 79, 'wdwde', '2018-08-28', '00:13:00.000000', '2018-08-28 16:39:18', '0000-00-00 00:00:00'),
(128, 80, '30 Aug 2018 talk again', '2018-08-30', '11:11:00.000000', '2018-08-29 07:13:54', '0000-00-00 00:00:00'),
(129, 80, 'gdhgd', '2018-08-31', '11:11:00.000000', '2018-08-30 07:24:49', '0000-00-00 00:00:00'),
(130, 80, 'ALi Raza', '2018-08-30', '11:11:00.000000', '2018-08-30 09:54:35', '0000-00-00 00:00:00'),
(131, 80, 'Ahmad', '2018-08-30', '00:12:00.000000', '2018-08-30 10:04:54', '0000-00-00 00:00:00'),
(132, 80, 'today meeting', '2018-08-30', '16:44:00.000000', '2018-08-30 10:06:05', '0000-00-00 00:00:00'),
(133, 79, 'Ali Raza 28 feed', '2018-08-28', '00:12:00.000000', '2018-08-30 10:12:00', '0000-00-00 00:00:00'),
(134, 81, 'today feed back\r\nramzan', '2018-08-30', '00:12:00.000000', '2018-08-30 10:19:07', '0000-00-00 00:00:00'),
(135, 83, 'Earum dolor explicabo Est ut qui occaecat dolor voluptatem', '2018-08-30', '00:00:00.000000', '2018-08-30 10:32:23', '0000-00-00 00:00:00'),
(136, 83, 'today is meeting', '2018-08-30', '11:11:00.000000', '2018-08-30 10:33:26', '0000-00-00 00:00:00'),
(137, 84, 'Et minima commodo non obcaecati corporis at mollitia rerum', '1971-02-11', '00:00:00.000000', '2018-08-29 10:47:56', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `follow_up_feedback`
--
ALTER TABLE `follow_up_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `follow_up_feedback`
--
ALTER TABLE `follow_up_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
